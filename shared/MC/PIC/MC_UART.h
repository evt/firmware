/*
 * File:    MC_UART.h
 * Created: 11/10/2016
*/

#ifndef MC_UART_H
#define MC_UART_H

#include "EVT_std_includes.h"

/* ========================================================================
 * Public Enumerated Types Definitions
 * ========================================================================
 */

/* These enums correspond with the io_uart_channel_E enums*/
/* Do not modify these values without changing them in the IO layer */
typedef enum mc_uart_channel_E
{
    MC_UART_CHANNEL1            = 1,
    MC_UART_CHANNEL2            = 2,
    MC_UART_NUM_CHANNELS        = 2,
} mc_uart_channel_E;

typedef enum mc_uart_interrupt_status_E
{
    MC_UART_INTERRUPT_DISABLED     = 0,
    MC_UART_INTERRUPT_ENABLED      = 1,
} mc_uart_interrupt_status_E;

typedef enum mc_uart_data_status_E
{
    MC_UART_NO_DATA        =  0,
    MC_UART_DATA_READY     =  1,
    MC_UART_OVERRUN_ERROR  =  2,
    MC_UART_FRAME_ERROR    =  3,
} mc_uart_data_status_E;

/* These enums correspond with the io_uart_channel_E enums*/
/* Do not modify these values without changing them in the IO layer */
typedef enum mc_uart_baud_E
{
    MC_UART_BAUD_9600      = 1,
    MC_UART_BAUD_19200     = 2,
    MC_UART_BAUD_38400     = 3,
    MC_UART_BAUD_57600     = 4,
    MC_UART_BAUD_115200    = 5
} mc_uart_baud_E;

/* ========================================================================
 * Public Function Declarations
 * ========================================================================
*/

/* Opens and configures a UART channel by setting the appropriate bits in
 * the special function registers. Currently supports two channels.
 *
 * Notes:
 *  - mc_uart_setBaud and mc_uart_setChannel must be called before
 *      init.
 */
void mc_uart_init();

/* Sets the baud rate to use when the uart module is next initialized
 *
 * Arguments:
 *  baud - the enum representing the desired baud rate for the UART module.
 *
 * Notes:
 *  - This function should be called before every call to init().
 */
void mc_uart_setBaud(mc_uart_baud_E baud);

/*
 * Closes the active UART channel
 */
void mc_uart_close();

/* Sends a byte of data to the UART peripheral to be transmitted. If the
 * channel is set up for 9-bit mode, it will also set the additional bit.
 *
 * Arguments:
 *  - data:    The byte to be written to the UART transmit registers.
 *
 * Returns:
 *  - None.
 *
 * Preconditions:
 *  - None. Function currently assumes configuration for 8-bit mode.
 *
 * Postconditions:
 *  - TXREG for the specified channel is written to.
*/
void mc_uart_writeData(uint8_t data);

/* Reads data from the UART channel's hardware data buffer.
 * 
 * Returns:
 *  - The byte of data received.
 * 
 * Preconditions:
 *  - There is data ready to be read from the peripheral.
 */
uint8_t mc_uart_readData();

/* Checks the UART registers to determine if a byte of data has been
 * received.
 * 
 * Arguments:
 *  - channel: The UART channel to check the flag for.
 * 
 * Returns:
 *  - An enum containing values for status of data in the received data buffer
 *    and errors in the transmission.
 */
mc_uart_data_status_E mc_uart_dataReady();

/* Checks the UART channel's hardware register to determine if the channel is
 * busy transmitting or is idle.
 *
 * Arguments:
 *  - channel: The UART channel to check the flag for.
 *
 * Returns:
 *  - A boolean representing whether the channel is busy or idle.
 */
bool mc_uart_channelBusy();

/* Checks whether the active UART channel is currently open.
 *
 * Returns:
 *  - A boolean representing whether the channel has been opened and
 *    configured or not.
 *
 * Preconditions:
 *  - None.
 */
bool mc_uart_isChannelOpen();

/* Enables the interrupt trigger when a byte is transmitted on the
 * UART channel specified.
 *
 * Arguments:
 *  - status: The setting for the Transmit Interrupt.
 *
 * Preconditions:
 *  - None.
 */
void mc_uart_setTransmitInterrupt(mc_uart_interrupt_status_E status);

/* Checks the interrupt trigger for a transmission on the active UART channel
 * is set.
 *
 * Preconditions:
 *  - None.
 */
mc_uart_interrupt_status_E mc_uart_getTransmitInterrupt();

/* Enables the interrupt trigger when a byte is received on the active UART
 * channel.
 *
 * Arguments:
 *  - status: The setting for the Receive Interrupt.
 *
 * preconditions:
 *  - none.
 */
void mc_uart_setReceiveInterrupt(mc_uart_interrupt_status_E status);

/* Checks if the interrupt trigger for a received byte on the UART channel is
 * set.
 *
 * Preconditions:
 *  - None.
 */
mc_uart_interrupt_status_E mc_uart_getReceiveInterrupt();

/* Gets the current baud rate for the UART channel.
 *
 * Preconditions:
 *  - None.
 */
mc_uart_baud_E mc_uart_getBaud();

/* Gets the current channel for the UART module.
 *
 * Preconditions:
 *  - None.
 */
mc_uart_baud_E mc_uart_getChannel();

#endif 	/* MC_UART_H */
