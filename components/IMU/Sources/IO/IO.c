/* 
 * File:   IO.c
 *
 * Created on September 8, 2016, 8:07 PM
 */

#include "IO.h"
#include "IO_timer.h"
#include "IO_CAN.h"
#include "IO_I2C.h"
#include "IO_UART.h"

int i;
void IO_init()
{
    //Initialize the IO layer
    io_timer_init(IO_TIMER_4, IO_TIMER_1MS);
    io_can_init();
    io_i2c_init();
    io_uart_init();
}

void IO_1ms()
{
    //Run any IO specific 1ms cyclic tasks
    io_uart_process();
}

void IO_10ms()
{
    //Run any IO specific 10ms cyclic tasks
    io_i2c_stateMachine();
}

void IO_100ms()
{
    //Run any IO specific 100ms cyclic tasks
}
