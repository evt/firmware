/*
 * File:    IO_ADS1158.c
 * Created: 3/31/2017
*/

#include "IO_ADS1158.h"
#include "IO_SPI.h"
#include "MC_board.h"
#include "MC_processor.h"
#include <string.h> //Included for memcpy

/* ===========================================================================================================================
 * Private Macro Definitions
 * ===========================================================================================================================
*/
#define ADS1158_LED_PERIOD (50) //Led blink period in ms

#define ADS1158_REF_VOLTAGE_DIVISOR (3072)
/* See page 24 of ADS1158 data sheet.
 * VREF = Voltage = code / 3072
 */

#define ADS1158_VOLTAGE_DIVISOR     (30720)
/* See page 23 of ADS1158 data sheet. 
 * 1LSB = VREF/30720
 * Voltage = code * VREF / 30720
 */

#define ADS1158_VOLTS_TO_MICROVOLTS   (1000000)
#define ADS1158_VOLTS_TO_MILLIVOLTS   (1000)

//#define ADS1158_GPIO_LED     (4)
//#define ADS1158_GPIO_START   (3)



#define ADS1158_CONFIG0_DEVICE_DEFAULT   (0x0A)
#define ADS1158_CONFIG1_DEVICE_DEFAULT   (0x83)
#define ADS1158_MUXSCH_DEVICE_DEFAULT    (0x00)
#define ADS1158_MUXDIF_DEVICE_DEFAULT    (0x00)
#define ADS1158_MUXSG0_DEVICE_DEFAULT    (0xFF)
#define ADS1158_MUXSG1_DEVICE_DEFAULT    (0xFF)
#define ADS1158_SYSRED_DEVICE_DEFAULT    (0x00)
#define ADS1158_GPIOC_DEVICE_DEFAULT     (0xFF)
#define ADS1158_GPIOD_DEVICE_DEFAULT     (0x00)
#define ADS1158_DEVICEID_DEVICE_DEFAULT  (0x9B)


/* ===========================================================================================================================
 * Private Enumerated type definitions
 * ===========================================================================================================================
*/

typedef enum ads1158_channel_enable_E
{
    ADS1158_CHANNEL_ENABLE_DEFAULT  = 0u,
    ADS1158_CHANNEL_DISABLED        = 0u, // Channel not selected within a reading sequence.
    ADS1158_CHANNEL_ENABLED         = 1u, // Channel selected within a reading sequence.
} ads1158_channel_enable_E;

typedef enum ads1158_register_E
{
    ADS1158_REGISTER_CONFIG0 = 0u,
    ADS1158_REGISTER_CONFIG1 = 1u,
    ADS1158_REGISTER_MUXSCH  = 2u,
    ADS1158_REGISTER_MUXDIF  = 3u,
    ADS1158_REGISTER_MUXSG0  = 4u,
    ADS1158_REGISTER_MUXSG1  = 5u,
    ADS1158_REGISTER_SYSRED  = 6u,
    ADS1158_REGISTER_GPIOC   = 7u,
    ADS1158_REGISTER_GPIOD   = 8u,
    ADS1158_REGISTER_ID      = 9u,
    ADS1158_NUM_REGISTERS,
    ADS1158_REGISTER_DONT_CARE = 0u,
} ads1158_register_E;

typedef enum ads1158_force_zero_E
{
    ADS1158_FORCED_ZERO  = 0u,
} ads1158_force_zero_E;

typedef enum ads1158_stat_byte_E
{
    ADS1158_STAT_BYTE_DEFAULT  = 1u,
    ADS1158_STAT_BYTE_DISABLED = 0u,   // Status Byte disabled
    ADS1158_STAT_BYTE_ENABLED  = 1u,   // Status Byte enabled (default)
} ads1158_stat_byte_E;

typedef enum ads1158_chop_E
{
    ADS1158_CHOP_DEFAULT  = 0u,
    ADS1158_CHOP_DISABLED = 0u,   // Disables the chopping feature on the ext multiplexor loop (default)
    ADS1158_CHOP_ENABLED  = 1u,   // Enables the chopping feature on the ext multiplexor loop
} ads1158_chop_E;

typedef enum ads1158_clkenb_E
{
    ADS1158_CLKENB_DEFAULT  = 1u,
    ADS1158_CLKENB_DISABLED = 0u,   // Disables the clock output on pin CLKIO
    ADS1158_CLKENB_ENABLED  = 1u,   // Enables the clock output on pin CLKIO (Default)
} ads1158_clkenb_E;

typedef enum ads1158_bypass_E
{
    ADS1158_BYPASS_DEFAULT  = 0u,
    ADS1158_BYPASS_DISABLED = 0u,   // ADC inputs use internal multiplexer connection (default)
    ADS1158_BYPASS_ENABLED  = 1u,   // ADC inputs use external ADC inputs (ADCINP and ADCINN)
} ads1158_bypass_E;

typedef enum ads1158_mux_mode_E
{
    ADS1158_MUXMOD_DEFAULT        = 0u,
    ADS1158_MUXMOD_AUTOSCAN       = 0u,   // Auto-Scan mode (default)
    ADS1158_MUXMOD_FIXED_CHANNEL  = 1u,   // Fixed-Channel mode
} ads1158_mux_mode_E;
   /* |  Mode          | Chan data Read command | chan data Read Direct |
    * -------------------------------------------------------------------
    * |  Autoscan      |     Always Enabled     |  En/Dis by STAT bit   |
    * |  Fixed Channel | Always Enabled (undef) |    Always Disabled    |
    */

typedef enum ads1158_SPI_reset_timer_E
{
    ADS1158_SPI_RESET_TIMER_DEFAULT  = 0u,
    ADS1158_SPI_RESET_TIMER_4096     = 0u,   // Reset when SCLK inactive for 4096fCLK cycles (256µs, fCLK = 16MHz) (default)
    ADS1158_SPI_RESET_TIMER_256      = 1u,   // Reset when SCLK inactive for 256fCLK cycles (16µs, fCLK = 16MHz).
} ads1158_SPI_reset_timer_E;

typedef enum ads1158_data_rate_E
{
    ADS1158_DATA_RATE_DEFAULT     = 3u,
    ADS1158_DATA_RATE_1821        = 0u, // Auto scan mode
    ADS1158_DATA_RATE_6168        = 1u, // Auto scan mode
    ADS1158_DATA_RATE_15123       = 2u, // Auto scan mode
    ADS1158_DATA_RATE_23739       = 3u, // Auto scan mode (Default)
    ADS1158_DATA_RATE_1953        = 0u, // Fixed channel mode
    ADS1158_DATA_RATE_7813        = 1u, // Fixed channel mode
    ADS1158_DATA_RATE_31250       = 2u, // Fixed channel mode
    ADS1158_DATA_RATE_125000      = 3u, // Fixed channel mode (Default)
} ads1158_data_rate_E;
/* Data rate of the converter. default = 11 */
/* | drate | sam/sec (auto-scan) | sam/sec (fixed-chan) |
 * |  11   |         23739       |          125000      |
 * |  10   |         15123       |           31250      |
 * |  01   |          6168       |            7813      |
 * |  00   |          1831       |            1953      |
 */

typedef enum ads1158_sensor_bias_source_E
{
    ADS1158_SENSOR_BIAS_DEFAULT    = 0u,
    ADS1158_SENSOR_BIAS_OFF        = 0u, // Sensor bias current source off (default)
    ADS1158_SENSOR_BIAS_1_5        = 1u, // 1.5µA source
    ADS1158_SENSOR_BIAS_24         = 3u, // 24µA source
} ads1158_sensor_bias_source_E;

typedef enum ads1158_converter_delay_E
/* Sets the amount of time to delay after indexing to a new channel, before converting 
 * Setting is in units of 128/(Fclock Periods) 
 */
{
    ADS1158_CONVERTER_DELAY_DEFAULT  = 0u,
    ADS1158_CONVERTER_DELAY_0        = 0u,
    ADS1158_CONVERTER_DELAY_1        = 1u,
    ADS1158_CONVERTER_DELAY_2        = 2u,
    ADS1158_CONVERTER_DELAY_4        = 3u,
    ADS1158_CONVERTER_DELAY_8        = 4u,
    ADS1158_CONVERTER_DELAY_16       = 5u,
    ADS1158_CONVERTER_DELAY_32       = 6u,
    ADS1158_CONVERTER_DELAY_48       = 7u,
} ads1158_converter_delay_E;

typedef enum ads1158_idle_mode_E
/* This bit selects the Idle mode when the device is not converting, Standby or Sleep. The Sleep mode
 * offers lower power consumption but has a longer wake-up time to re-enter the run mode
 */
{
    ADS1158_IDLE_MODE_DEFAULT     = 1u,
    ADS1158_IDLE_MODE_STANDBY     = 0u, // Select standby mode
    ADS1158_IDLE_MODE_SLEEP       = 1u, // Select sleep mode (default)
} ads1158_idle_mode_E;

typedef enum ads1158_device_id_E
{
    ADS1158_DEVICE_ADS1258      = 0u, // ADS1258 (24-bit ADC)
    ADS1158_DEVICE_ADS1158      = 1u, // ADS1158 (16-bit ADC)
} ads1158_device_id_E;

typedef enum ads1158_new_status_E
{
    ADS1158_OLD_DATA     = 0u, // Channel data have not been updated since the last read operation.
    ADS1158_NEW_DATA     = 1u, // Channel data have been updated since the last read operation.
} ads1158_new_status_E;

typedef enum ads1158_over_voltage_flag_E
{
    ADS1158_VOLTAGE_IN_RANGE  = 0u, // The differential voltage applied to the inputs is within standard operating range.
    ADS1158_OVER_VOLTAGE      = 1u, // The differential voltage applied to the ADC inputs have exceeded the range of the converter |VIN| > 1.06VREF
} ads1158_over_voltage_flag_E;

typedef enum ads1158_supply_under_voltage_E
{
    ADS1158_SUPPLY_GOOD           = 0u, // The (AVDD �?? AVSS) value is above 4.3V
    ADS1158_SUPPLY_UNDER_VOLTAGE  = 1u, // The (AVDD �?? AVSS) value has fallen below 4.3V
} ads1158_supply_under_voltage_E;

typedef enum ads1158_command_E
{
    ADS1158_COMMAND_CHANNEL_DATA_READ_DIRECT   = 0u, // Toggle CS or allow SPI timeout before sending command
    ADS1158_COMMAND_CHANNEL_DATA_READ_COMMAND  = 1u, // Set MUL = 1; status byte always included in data
    ADS1158_COMMAND_REGISTER_READ_COMMAND      = 2u, // A[3:0] = 0000
    ADS1158_COMMAND_REGISTER_WRITE_COMMAND     = 3u, //
    ADS1158_COMMAND_PULSE_CONVERT_COMMAND      = 4u, // MUL, A[3:0] are don't care
    ADS1158_COMMAND_RESERVED                   = 5u, //
    ADS1158_COMMAND_RESET_COMMAND              = 6u, // MUL, A[3:0] don't care
    // There is a copy of ADS1158_COMMAND_CHANNEL_DATA_READ_DIRECT at 7u
} ads1158_command_E;

typedef enum ads1158_multiple_register_access_E
{
    ADS1158_MULTIPLE_REGISTER_ACCESS_DISABLED = 0u, // Disable Multiple Register Access
    ADS1158_MULTIPLE_REGISTER_ACCESS_ENABLED  = 1u, // Enable Multiple Register Access
} ads1158_multiple_register_access_E;

/* ===========================================================================================================================
 * Private Structure definitions
 * ===========================================================================================================================
*/

//don't forget structs go LSB first
typedef union ads1158_config0_S
{
    uint8_t reg; //device default = 0x0A
    struct //register bitfield definition
    {
        //lsb
        uint8_t zeroLSB :1; // ads1158_force_zero_E
        uint8_t stat    :1; // ads1158_stat_byte_E
        uint8_t chop    :1; // ads1158_chop_E
        uint8_t clkenb  :1; // ads1158_clkenb_E
        uint8_t bypass  :1; // ads1158_bypass_E
        uint8_t muxmod  :1; // ads1158_mux_mode_E
        uint8_t spirst  :1; // ads1158_SPI_reset_timer_E
        uint8_t zeroMSB :1; // ads1158_force_zero_E
        //msb
    };
} ads1158_config0_S;

typedef union ads1158_config1_S
{
    uint8_t reg; //device default = 0x83
    struct //register bitfield definition
    {
        //lsb
        uint8_t drate  :2; // ads1158_data_rate_E
        uint8_t sbcs   :2; // ads1158_sensor_bias_source_E
        uint8_t dly    :3; // ads1158_converter_delay_E
        uint8_t idlmod :1; // ads1158_idle_mode_E
        //msb
    };
} ads1158_config1_S;

typedef union ads1158_muxsch_S
{
    uint8_t reg; //device default = 0x00
    struct //register bitfield definition
    {
        //lsb
        uint8_t AINN :4; //Selects the analog input channel for the negative ADC input
        uint8_t AINP :4; //Selects the analog input channel for the positive ADC input
        //msb
    };
} ads1158_muxsch_S;

typedef union ads1158_muxdif_S
{
    uint8_t reg; //device default = 0x00
    struct //register bitfield definition
    {
        //lsb
        uint8_t diff0 :1; //Select the diff 0 channel for auto-scan mode - ads1158_channel_enable_E
        uint8_t diff1 :1; //Select the diff 1 channel for auto-scan mode - ads1158_channel_enable_E
        uint8_t diff2 :1; //Select the diff 2 channel for auto-scan mode - ads1158_channel_enable_E
        uint8_t diff3 :1; //Select the diff 3 channel for auto-scan mode - ads1158_channel_enable_E
        uint8_t diff4 :1; //Select the diff 4 channel for auto-scan mode - ads1158_channel_enable_E
        uint8_t diff5 :1; //Select the diff 5 channel for auto-scan mode - ads1158_channel_enable_E
        uint8_t diff6 :1; //Select the diff 6 channel for auto-scan mode - ads1158_channel_enable_E
        uint8_t diff7 :1; //Select the diff 7 channel for auto-scan mode - ads1158_channel_enable_E
        //msb
    };
} ads1158_muxdif_S;

typedef union ads1158_muxsg0_S
{
    uint8_t reg; //device default = 0xFF
    struct //register bitfield definition
    {
        //lsb
        uint8_t ain0 :1; //Select the AIN 0 channel for auto-scan mode - ads1158_channel_enable_E
        uint8_t ain1 :1; //Select the AIN 1 channel for auto-scan mode - ads1158_channel_enable_E
        uint8_t ain2 :1; //Select the AIN 2 channel for auto-scan mode - ads1158_channel_enable_E
        uint8_t ain3 :1; //Select the AIN 3 channel for auto-scan mode - ads1158_channel_enable_E
        uint8_t ain4 :1; //Select the AIN 4 channel for auto-scan mode - ads1158_channel_enable_E
        uint8_t ain5 :1; //Select the AIN 5 channel for auto-scan mode - ads1158_channel_enable_E
        uint8_t ain6 :1; //Select the AIN 6 channel for auto-scan mode - ads1158_channel_enable_E
        uint8_t ain7 :1; //Select the AIN 7 channel for auto-scan mode - ads1158_channel_enable_E
        //msb
    };
} ads1158_muxsg0_S;

typedef union ads1158_muxsg1_S
{
    uint8_t reg; //device default = 0xFF
    struct //register bitfield definition
    {
        //lsb
        uint8_t ain8 :1; //Select the AIN 8 channel for auto-scan mode   - ads1158_channel_enable_E
        uint8_t ain9 :1; //Select the AIN 9 channel for auto-scan mode   - ads1158_channel_enable_E
        uint8_t ain10 :1; //Select the AIN 10 channel for auto-scan mode - ads1158_channel_enable_E
        uint8_t ain11 :1; //Select the AIN 11 channel for auto-scan mode - ads1158_channel_enable_E
        uint8_t ain12 :1; //Select the AIN 12 channel for auto-scan mode - ads1158_channel_enable_E
        uint8_t ain13 :1; //Select the AIN 13 channel for auto-scan mode - ads1158_channel_enable_E
        uint8_t ain14 :1; //Select the AIN 14 channel for auto-scan mode - ads1158_channel_enable_E
        uint8_t ain15 :1; //Select the AIN 15 channel for auto-scan mode - ads1158_channel_enable_E
        //msb
    };
} ads1158_muxsg1_S;

typedef union ads1158_sysred_S
{
    uint8_t reg; //device default = 0x00
    struct //register bitfield definition
    {
        //lsb
        uint8_t offset :1;  //Select the offset channel for auto-scan mode - ads1158_channel_enable_E
                            /* The differential output of the multiplexer is shorted together and set to a common-mode voltage of 
                             * (AVDD - AVSS)/2. Ideally, the code from this register function is 0h, but varies because of the noise of the
                             * ADC and offsets stemming from the ADC and external signal conditioning. This register can be used
                             * to calibrate or track the offset of the ADS1158 and external signal conditioning.
                             */
        uint8_t zero0  :1;  //Must be 0
        uint8_t vcc    :1;  //Select the vcc channel for auto-scan mode    - ads1158_channel_enable_E
        uint8_t temp   :1;  //Select the temp channel for auto-scan mode   - ads1158_channel_enable_E
        uint8_t gain   :1;  //Select the gain channel for auto-scan mode   - ads1158_channel_enable_E
        uint8_t ref    :1;  //Select the ref channel for auto-scan mode    - ads1158_channel_enable_E
        uint8_t zero1  :1;  //Must be 0
        uint8_t zero2  :1;  //Must be 0
        //msb
    };
} ads1158_sysred_S;

typedef union ads1158_gpioc_S
{
    uint8_t reg; //device default = 0xFF
    struct //register bitfield definition
    {
        //lsb
        uint8_t cio0 :1; //digital I/O configuration bit for pin GPIO0 - ads1158_GPIO_direction_E
        uint8_t cio1 :1; //digital I/O configuration bit for pin GPIO1 - ads1158_GPIO_direction_E
        uint8_t cio2 :1; //digital I/O configuration bit for pin GPIO2 - ads1158_GPIO_direction_E
        uint8_t cio3 :1; //digital I/O configuration bit for pin GPIO3 - ads1158_GPIO_direction_E
        uint8_t cio4 :1; //digital I/O configuration bit for pin GPIO4 - ads1158_GPIO_direction_E
        uint8_t cio5 :1; //digital I/O configuration bit for pin GPIO5 - ads1158_GPIO_direction_E
        uint8_t cio6 :1; //digital I/O configuration bit for pin GPIO6 - ads1158_GPIO_direction_E
        uint8_t cio7 :1; //digital I/O configuration bit for pin GPIO7 - ads1158_GPIO_direction_E
        //msb
    };
} ads1158_gpioc_S;

typedef union ads1158_gpiod_S
{
    uint8_t reg; //device default = 0x00
    struct //register bitfield definition
    {
        //lsb
        uint8_t dio0 :1; //digital I/O data bit for pin GPIO0 - ads1158_GPIO_value_E
        uint8_t dio1 :1; //digital I/O data bit for pin GPIO1 - ads1158_GPIO_value_E
        uint8_t dio2 :1; //digital I/O data bit for pin GPIO2 - ads1158_GPIO_value_E
        uint8_t dio3 :1; //digital I/O data bit for pin GPIO3 - ads1158_GPIO_value_E
        uint8_t dio4 :1; //digital I/O data bit for pin GPIO4 - ads1158_GPIO_value_E
        uint8_t dio5 :1; //digital I/O data bit for pin GPIO5 - ads1158_GPIO_value_E
        uint8_t dio6 :1; //digital I/O data bit for pin GPIO6 - ads1158_GPIO_value_E
        uint8_t dio7 :1; //digital I/O data bit for pin GPIO7 - ads1158_GPIO_value_E
        //msb
    };
} ads1158_gpiod_S;

typedef union ads1158_device_id_S
{
    uint8_t reg; //device default = 0x9B
    struct //register bitfield definition
    {
        //lsb
        uint8_t id0 :1; //Factory-programmed ID bits. Read-only.
        uint8_t id1 :1; //Factory-programmed ID bits. Read-only.
        uint8_t id2 :1; //Factory-programmed ID bits. Read-only.
        uint8_t id3 :1; //Factory-programmed ID bits. Read-only.
        uint8_t id4 :1; //Factory-programmed ID bits. Read-only. - ads1158_device_id_E
        uint8_t id5 :1; //Factory-programmed ID bits. Read-only.
        uint8_t id6 :1; //Factory-programmed ID bits. Read-only.
        uint8_t id7 :1; //Factory-programmed ID bits. Read-only.
        //msb
    };
} ads1158_device_id_S;


typedef struct ads1158_config_registers_S
{
    //lsb
    ads1158_config0_S    config0;
    ads1158_config1_S    config1;
    ads1158_muxsch_S     muxsch;
    ads1158_muxdif_S     muxdif;
    ads1158_muxsg0_S     muxsg0;
    ads1158_muxsg1_S     muxsg1;
    ads1158_sysred_S     sysred;
    ads1158_gpioc_S      GPIOc;
    ads1158_gpiod_S      GPIOd;
    ads1158_device_id_S  deviceID;
    //msb
} ads1158_config_registers_S;


//don't forget structs go LSB first
typedef struct ads1158_channel_data_S
{
//  float voltage; //This is the converted voltage for for the channel
    union
    {
        struct
        {
            int16_t data; //full 16 bit voltage data value. Stored in Binary Twos Compliment form
        };
        struct
        {
            uint8_t dataLSB; //second recieved byte of the voltage data
            uint8_t dataMSB; //first recieved byte of the voltage data
        };
    };

    union
    {
        struct
        {
            uint8_t status; // full status byte
        };
        struct //breakdown of status byte into important bits
        {
            uint8_t channelID                :5; //ads1158_channel_E

            /* This bit is reset when the value rises 50mV higher (typically) than the lower trip point. 
             * The output data of the ADC may not be valid under low power-supply conditions. */
            uint8_t powerSupplyUnderVoltage  :1; // ads1158_supply_under_voltage_E
            
            /* During over-range, the output code of the converter clips to either positive FS (VIN �?� 1.06 �? VREF) or negative FS (VIN �?� �??1.06 �? VREF). 
             * This bit, with the MSB of the data, can be used to detect positive or negative over-range conditions. */
            uint8_t overVoltageFlag          :1; // ads1158_over_voltage_flag_E 
            uint8_t newFlag                  :1; // ads1158_new_status_E
        };
    };
} ads1158_channel_data_S;

typedef union ads1158_command_byte_S
{
    uint8_t command;
    struct //register bitfield definition
    {
        //lsb
        uint8_t registerAddress         :4; // ads1158_register_E
        uint8_t multipleRegisterAccess  :1; // ads1158_multiple_register_access_E
        uint8_t commandBits             :3; // ads1158_command_E
        //msb
    };
} ads1158_command_byte_S;


/* ===========================================================================================================================
 * Private module variables
 * ===========================================================================================================================
*/

/* Current state machine state of the motion module */
static ads1158_state_E ads1158CurrentState;
static ads1158_state_E ads1158NextState;
static bool ads1158StateChanged;
static bool ads1158ConfigChanged;
static bool ads1158LEDActive;
static ads1158_GPIO_value_E ads1158CurrentLEDState;
static uint8_t ads1158LEDTimer;


/* Store current config registers */
static ads1158_config_registers_S ads1158CurrentConfigs;

/* Store current conversion values */
static ads1158_channel_data_S ads1158CurrentConversionValues[ADS1158_NUM_CHANNEL_IDS];

/* Store current channel voltage values */
static uint16_t ads1158CurrentMilliVoltageValues[ADS1158_NUM_CHANNEL_IDS]; // Voltage of each channel stored in mV

static uint32_t ads1158CurrentMicroVoltageValue; // Voltage of 1 channel stored in uV

/* Store current channel enable status */
static ads1158_channel_enable_E ads1158EnabledChannels[ADS1158_NUM_CHANNEL_IDS];
static uint8_t ads1158NumEnabledChannels;

static ads1158_read_precision_E ads1158readPrecision;



/* ===========================================================================================================================
 * Private module function declarations
 * ===========================================================================================================================
*/
void ads1158_enterState();
void ads1158_processState();
void ads1158_exitState();
void ads1158_writeConfigs();
uint8_t ads1158_verifyConfigs();
void ads1158_startConverter();
uint8_t ads1158DataReady();
void ads1158_readConversions(uint8_t numChannels);
void ads1158_convertmVoltages();
void ads1158_convertuVoltages();
void ads1158_defaultGPIOConfig();
void ads1158_defaultEnabledChannels();
void ads1158_countEnabledChannels();
void ads1158_defaultConfigRegisters();
void ads1158_setConfigChannelRegisters();
void ads1158_setPrecision(ads1158_read_precision_E precision);

/* ===========================================================================================================================
 *  Module function definitions
 * ===========================================================================================================================
*/

void io_ads1158_init()
{
    
    /* Setup Chip select pic */
    io_spi_openCS(MC_BOARD_CS_ADC);
    /* setup input pins */
    MC_pin_setDirection(MC_BOARD_DRDY, MC_PIN_INPUT);
    /* setup output pins */
    MC_pin_setDirection(MC_BOARD_ADCSTART, MC_PIN_OUTPUT);
    MC_pin_setDirection(MC_BOARD_ADC_RESET, MC_PIN_OUTPUT);
    MC_pin_setDirection(MC_BOARD_PWDN, MC_PIN_OUTPUT);
    
    /* Start the Reset and Power down pins high so the device turns on */
    //Turn into functions!
    MC_GPIO_write(MC_BOARD_ADC_RESET, MC_PIN_HIGH);
    MC_GPIO_write(MC_BOARD_PWDN, MC_PIN_HIGH);

    ads1158CurrentState = ADS1158_STATE_RESET_SPI;
    ads1158NextState    = ADS1158_STATE_RESET_SPI;
    ads1158StateChanged = false;
    ads1158LEDActive = false;
    ads1158LEDTimer = 0;

    /* Set up the stored config registers and enable ads1158ConfigChanged*/
    ads1158_defaultConfigRegisters();
    
    /* Setup ADS1158 on board GPIOs */
    io_ads1158_setGPIODirection(ADS1158_GPIO_LED, ADS1158_GPIO_DIRECTION_OUTPUT);
    ads1158CurrentLEDState = ADS1158_GPIO_VALUE_LOW;
    io_ads1158_GPIOwrite(ADS1158_GPIO_LED, ads1158CurrentLEDState);
    
    io_ads1158_setGPIODirection(ADS1158_GPIO_START, ADS1158_GPIO_DIRECTION_OUTPUT);
    io_ads1158_GPIOwrite(ADS1158_GPIO_START, ADS1158_GPIO_VALUE_LOW);
    
    ads1158_setPrecision(ADS1158_READ_PRECISION_MICROVOLTS);
}


void io_ads1158_run()
{   
    //entry functions --------------------------------------------------------------
    if (ads1158StateChanged)
    {
        ads1158_enterState();
        ads1158StateChanged = false;
    }
    else
    {
        // There was no state change, don't run the entry functions
    }

    //periodic functions -----------------------------------------------------------
    ads1158_processState();

    //exit functions ---------------------------------------------------------------
    if (ads1158StateChanged)
    {
        ads1158_exitState();
    }
    else
    {
        // There was no state change, don't run the exit functions
    }

    /* "Heartbeat" LED blink */
    if (ads1158LEDActive)
    {
        ads1158LEDTimer++;
        if (ads1158LEDTimer >= ADS1158_LED_PERIOD)
        {
            ads1158CurrentLEDState = !ads1158CurrentLEDState;
            io_ads1158_GPIOwrite(ADS1158_GPIO_LED, ads1158CurrentLEDState);

            ads1158LEDTimer = 0;
        }
    }
}


void ads1158_enterState()
{
    //kick off any asynchronous tasks for each state
    switch(ads1158CurrentState)
    {
        case ADS1158_STATE_RESET_SPI:
            
            break;
        case ADS1158_STATE_RESET:
            io_spi_toggleCSLow(MC_BOARD_CS_ADC);
            break;
        case ADS1158_STATE_SETUP_CONFIG:
            
            break;
        case ADS1158_STATE_READ_DATA:
            
            break;

        /*
         * We should never get here
        */
        default:
            break;
    }
}


void ads1158_processState()
{
    switch(ads1158CurrentState)
    {
        case ADS1158_STATE_RESET_SPI:
            ads1158LEDActive = false;
            io_spi_toggleCSHigh(MC_BOARD_CS_ADC);

            ads1158NextState = ADS1158_STATE_RESET;
            break;
            
        case ADS1158_STATE_RESET:
            MC_GPIO_write(MC_BOARD_ADCSTART, MC_PIN_LOW);
            MC_GPIO_write(MC_BOARD_ADC_RESET, MC_PIN_LOW);

            ads1158NextState = ADS1158_STATE_SETUP_CONFIG;
            break;
            
        case ADS1158_STATE_SETUP_CONFIG:
            /* Write to config registers */
            ads1158_writeConfigs();
        	/* Check if config registers wrote correctly */
            if (ads1158_verifyConfigs())
            {
                ads1158NextState = ADS1158_STATE_READ_DATA;
            }
            else
            {
                //do nothing, stay in this state
                //maybe set and error?
            }

            break;

        case ADS1158_STATE_READ_DATA:
            /* Check if any configs got updated */
            if(ads1158ConfigChanged)
            {
                ads1158_writeConfigs();
                ads1158ConfigChanged = false;
            }

            /* read in data
             * This takes 900us for 4 channels confirmed with the oscilloscope
             * 300 us for 1 channel - Oscope confirmed
             */
            ads1158_readConversions(ads1158NumEnabledChannels);
            
            if (ads1158readPrecision == ADS1158_READ_PRECISION_MICROVOLTS)
            {
                ads1158_convertuVoltages();
            }
            else if (ads1158readPrecision == ADS1158_READ_PRECISION_MILLIVOLTS)
            {
                ads1158_convertmVoltages();
            }

            break;
            
        default:
            /*
             * We should never get here, if we do we set the state enum to an illegal value, reset it to a safe state
            */
            break;
    }
    
    //if we changed the state, make sure we let the exit and entry functions know!
    if (ads1158CurrentState != ads1158NextState)
    {
        ads1158StateChanged = true;
    }
}

void ads1158_exitState()
{
    switch(ads1158CurrentState)
    {
        case ADS1158_STATE_RESET_SPI:

            break;
        case ADS1158_STATE_RESET:
            MC_GPIO_write(MC_BOARD_ADC_RESET, MC_PIN_HIGH);
            break;
        case ADS1158_STATE_SETUP_CONFIG:
            /* Configuration and setup has successfully completed, blink the LED */
            ads1158LEDActive = true;
            
            break;
        case ADS1158_STATE_READ_DATA:

            break;

        /*
         * We should never get here
        */
        default:
            break;
    }
    
    //update the current state to the next desired state!
    ads1158CurrentState = ads1158NextState;
}

// oops might need to make states public? - nope
ads1158_state_E io_ads1158_getState()
{
    return ads1158CurrentState;
}

void ads1158_writeConfigs()
{
    ads1158_command_byte_S command;

    /* Setup the command to be sent */
    command.commandBits = ADS1158_COMMAND_REGISTER_WRITE_COMMAND;
    command.multipleRegisterAccess = ADS1158_MULTIPLE_REGISTER_ACCESS_ENABLED;
    command.registerAddress = ADS1158_REGISTER_CONFIG0;

    /* Write the command */
    io_spi_toggleCSLow(MC_BOARD_CS_ADC);
    io_spi_write(&command.command, 1);

    /* Write each config register */
    io_spi_write(&(ads1158CurrentConfigs.config0.reg), 1);
    io_spi_write(&(ads1158CurrentConfigs.config1.reg), 1);
    io_spi_write(&(ads1158CurrentConfigs.muxsch.reg), 1);
    io_spi_write(&(ads1158CurrentConfigs.muxdif.reg), 1);
    io_spi_write(&(ads1158CurrentConfigs.muxsg0.reg), 1);
    io_spi_write(&(ads1158CurrentConfigs.muxsg1.reg), 1);
    io_spi_write(&(ads1158CurrentConfigs.sysred.reg), 1);
    io_spi_write(&(ads1158CurrentConfigs.GPIOc.reg), 1);
    io_spi_write(&(ads1158CurrentConfigs.GPIOd.reg), 1);
    /* Cannot write to the ID register */

    io_spi_toggleCSHigh(MC_BOARD_CS_ADC);
}

uint8_t ads1158_verifyConfigs()
{
    uint8_t deviceConfigRegisters[ADS1158_NUM_REGISTERS];
    ads1158_command_byte_S command;
    uint8_t returnValue = 1u;
    uint8_t i;

    /* Setup the command to be sent */
    command.commandBits = ADS1158_COMMAND_REGISTER_READ_COMMAND;
    command.multipleRegisterAccess = ADS1158_MULTIPLE_REGISTER_ACCESS_ENABLED;
    command.registerAddress = ADS1158_REGISTER_CONFIG0;
    
    /* write the read command */
    io_spi_toggleCSLow(MC_BOARD_CS_ADC);
    io_spi_write(&(command.command), 1);

    /*read all of the config registers */
    io_spi_read(deviceConfigRegisters, ADS1158_NUM_REGISTERS);

    for(i = 0; i < ADS1158_NUM_REGISTERS; i++)
    {
        if (0 == i)
        {
            returnValue &= (deviceConfigRegisters[0] == ads1158CurrentConfigs.config0.reg);
        }

        if (1 == i)
        {
            returnValue &= (deviceConfigRegisters[1] == ads1158CurrentConfigs.config1.reg);
        }
        
        if (2 == i)
        {
            returnValue &= (deviceConfigRegisters[2] == ads1158CurrentConfigs.muxsch.reg);
        }

        if (3 == i)
        {
            returnValue &= (deviceConfigRegisters[3] == ads1158CurrentConfigs.muxdif.reg);
        }
        
        if (4 == i)
        {
            returnValue &= (deviceConfigRegisters[4] == ads1158CurrentConfigs.muxsg0.reg);
        }
        
        if (5 == i)
        {
            returnValue &= (deviceConfigRegisters[5] == ads1158CurrentConfigs.muxsg1.reg);
        }
        
        if (6 == i)
        {
            returnValue &= (deviceConfigRegisters[6] == ads1158CurrentConfigs.sysred.reg);
        }
        
        if (7 == i)
        {
            returnValue &= (deviceConfigRegisters[7] == ads1158CurrentConfigs.GPIOc.reg);
        }
        
        if (8 == i)
        {
            returnValue &= (deviceConfigRegisters[8] == ads1158CurrentConfigs.GPIOd.reg);
        }

        /* Don't check the device ID register as it is not gaurenteed to be anything and we cannot write to it
        if (9 == i)
        {
            returnValue &= (deviceConfigRegisters[9] == ads1158CurrentConfigs.deviceID.reg);
        }
        */
    }
    io_spi_toggleCSHigh(MC_BOARD_CS_ADC);

    return returnValue;
}


void ads1158_startConverter()
{
    //MC_GPIO_write(MC_BOARD_ADCSTART, MC_PIN_HIGH);
    //MC_GPIO_write(MC_BOARD_ADCSTART, MC_PIN_LOW);
    LATBbits.LATB4 = 1U;
    LATBbits.LATB4 = 0U;
}


uint8_t ads1158DataReady()
{
    return (MC_PIN_LOW == MC_GPIO_read(MC_BOARD_DRDY));
    //return (MC_PIN_LOW == (MC_PIN_STATES_E)PORTAbits.RA5);
}


void ads1158_readConversions(uint8_t numChannels)
{
    static ads1158_command_byte_S command;
    uint8_t i;
    ads1158_channel_data_S tempStatus;
    uint8_t channelID;

    /* Setup the command to be sent */
    command.commandBits = ADS1158_COMMAND_CHANNEL_DATA_READ_COMMAND;
    command.multipleRegisterAccess = ADS1158_MULTIPLE_REGISTER_ACCESS_ENABLED;
    command.registerAddress = ADS1158_REGISTER_DONT_CARE;
    
    /* read all of the relevant registers */
    for(i = 0; i < numChannels; i++)
    {   
        /* Must do a start pin pulse BEFORE the read command in order to increment the ADC to the next channel before reading! */
        /* Must also do a start pin pulse BEFORE each channel read. 1 start pin pulse = 1 channel converted */
        /* Start Converter */
        ads1158_startConverter();
        while (!ads1158DataReady()) 
        {
        /* do nothing - waiting for the data to be ready */
        } 
        
        /* On the First read, make sure we send the read command first */
        if (0 == i)
        {
            /* write the read command */
            io_spi_toggleCSLow(MC_BOARD_CS_ADC);
            io_spi_write(&(command.command), 1);
        }
        
        /* See page 29 of the data sheet for the order that the ADS clocks data out */
        io_spi_read(&(tempStatus.status), 1);
        
        /* Get the Channel ID from the status Byte, use it to make sure the data gets set to the correct channel in storage */
        channelID = tempStatus.channelID;
        ads1158CurrentConversionValues[channelID].status = tempStatus.status;
        io_spi_read(&(ads1158CurrentConversionValues[channelID].dataMSB), 1);
        io_spi_read(&(ads1158CurrentConversionValues[channelID].dataLSB), 1);
    }
    io_spi_toggleCSHigh(MC_BOARD_CS_ADC);
}

void ads1158_convertmVoltages()
{
    uint8_t i;
    /* Convert the reference voltage first 
     * VREF = Voltage = code / 3072
     */
    //ads1158CurrentVoltageValues[ADS1158_CHANNEL_ID_REF] = ( ( ads1158CurrentConversionValues[ADS1158_CHANNEL_ID_REF].data * ADS1158_VOLTS_TO_MICROVOLTS ) / ADS1158_REF_VOLTAGE_DIVISOR );

    /* Convert all channel voltages *EXCEPT* for reference channel
     * Voltage = code * VREF / 30720
     */
    for(i = ADS1158_CHANNEL_ID_AIN1; i < ADS1158_CHANNEL_ID_AIN15; i++)
    {
        ads1158CurrentMilliVoltageValues[i] = ( ( ( ((uint16_t)ads1158CurrentConversionValues[i].data) >> 6) * ((uint16_t)125) ) / ((uint16_t)12) );
    }
}

void ads1158_convertuVoltages()
{
    /* Convert the reference voltage first 
     * VREF = Voltage = code / 3072
     */
    //ads1158CurrentVoltageValues[ADS1158_CHANNEL_ID_REF] = ( ( ads1158CurrentConversionValues[ADS1158_CHANNEL_ID_REF].data * ADS1158_VOLTS_TO_MICROVOLTS ) / ADS1158_REF_VOLTAGE_DIVISOR );

    /* Convert all channel voltages *EXCEPT* for reference channel
     * Voltage = code * VREF / 30720
     */

    ads1158CurrentMicroVoltageValue = ( ( ((uint32_t)(ads1158CurrentConversionValues[ADS1158_CHANNEL_ID_AIN0].data)) * ((uint32_t)15625) ) / ((uint32_t)96) );
}


void ads1158_defaultGPIOConfig()
{
    ads1158CurrentConfigs.GPIOc.cio0 = ADS1158_GPIO_DIRECTION_INPUT;
    ads1158CurrentConfigs.GPIOc.cio1 = ADS1158_GPIO_DIRECTION_INPUT;
    ads1158CurrentConfigs.GPIOc.cio2 = ADS1158_GPIO_DIRECTION_INPUT;
    ads1158CurrentConfigs.GPIOc.cio3 = ADS1158_GPIO_DIRECTION_INPUT;
    ads1158CurrentConfigs.GPIOc.cio4 = ADS1158_GPIO_DIRECTION_INPUT;
    ads1158CurrentConfigs.GPIOc.cio5 = ADS1158_GPIO_DIRECTION_INPUT;
    ads1158CurrentConfigs.GPIOc.cio6 = ADS1158_GPIO_DIRECTION_INPUT;
    ads1158CurrentConfigs.GPIOc.cio7 = ADS1158_GPIO_DIRECTION_INPUT;

    ads1158CurrentConfigs.GPIOd.dio0 = ADS1158_GPIO_VALUE_LOW;
    ads1158CurrentConfigs.GPIOd.dio1 = ADS1158_GPIO_VALUE_LOW;
    ads1158CurrentConfigs.GPIOd.dio2 = ADS1158_GPIO_VALUE_LOW;
    ads1158CurrentConfigs.GPIOd.dio3 = ADS1158_GPIO_VALUE_LOW;
    ads1158CurrentConfigs.GPIOd.dio4 = ADS1158_GPIO_VALUE_LOW;
    ads1158CurrentConfigs.GPIOd.dio5 = ADS1158_GPIO_VALUE_LOW;
    ads1158CurrentConfigs.GPIOd.dio6 = ADS1158_GPIO_VALUE_LOW;
    ads1158CurrentConfigs.GPIOd.dio7 = ADS1158_GPIO_VALUE_LOW;
}

void ads1158_defaultEnabledChannels()
{
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_DIFF0]  = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_DIFF1]  = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_DIFF2]  = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_DIFF3]  = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_DIFF4]  = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_DIFF5]  = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_DIFF6]  = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_DIFF7]  = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN0]   = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN1]   = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN2]   = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN3]   = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN4]   = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN5]   = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN6]   = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN7]   = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN8]   = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN9]   = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN10]  = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN11]  = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN12]  = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN13]  = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN14]  = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN15]  = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_OFFSET] = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_VCC]    = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_TEMP]   = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_GAIN]   = ADS1158_CHANNEL_DISABLED;
    ads1158EnabledChannels[ADS1158_CHANNEL_ID_REF]    = ADS1158_CHANNEL_DISABLED;
    ads1158_countEnabledChannels();
}

void ads1158_countEnabledChannels()
{
    uint8_t i;
    ads1158NumEnabledChannels = 0;
    for(i = 0; i < ADS1158_NUM_CHANNEL_IDS; i++)
    {
        if (ads1158EnabledChannels[i] == ADS1158_CHANNEL_ENABLED)
        {
            ads1158NumEnabledChannels++;
        }
    }
}

void ads1158_defaultConfigRegisters()
{
    /* Set up initial config registers as device default values */
    ads1158CurrentConfigs.config0.reg  = ADS1158_CONFIG0_DEVICE_DEFAULT;
    ads1158CurrentConfigs.config1.reg  = ADS1158_CONFIG1_DEVICE_DEFAULT;
    ads1158CurrentConfigs.muxsch.reg   = ADS1158_MUXSCH_DEVICE_DEFAULT;
    ads1158CurrentConfigs.muxdif.reg   = ADS1158_MUXDIF_DEVICE_DEFAULT;
    ads1158CurrentConfigs.muxsg0.reg   = ADS1158_MUXSG0_DEVICE_DEFAULT;
    ads1158CurrentConfigs.muxsg1.reg   = ADS1158_MUXSG1_DEVICE_DEFAULT;
    ads1158CurrentConfigs.sysred.reg   = ADS1158_SYSRED_DEVICE_DEFAULT;
    ads1158CurrentConfigs.GPIOc.reg    = ADS1158_GPIOC_DEVICE_DEFAULT;
    ads1158CurrentConfigs.GPIOd.reg    = ADS1158_GPIOD_DEVICE_DEFAULT;
    ads1158CurrentConfigs.deviceID.reg = ADS1158_DEVICEID_DEVICE_DEFAULT;


    /* Set up the config0 register to our specific settings */
    ads1158CurrentConfigs.config0.stat   = ADS1158_STAT_BYTE_ENABLED;
    ads1158CurrentConfigs.config0.chop   = ADS1158_CHOP_DISABLED;
    ads1158CurrentConfigs.config0.clkenb = ADS1158_CLKENB_DISABLED;
    ads1158CurrentConfigs.config0.bypass = ADS1158_BYPASS_DISABLED;
    ads1158CurrentConfigs.config0.muxmod = ADS1158_MUXMOD_AUTOSCAN;
    ads1158CurrentConfigs.config0.spirst = ADS1158_SPI_RESET_TIMER_4096;

    /* Set up the config1 register to our specific settings */
    ads1158CurrentConfigs.config1.drate  = ADS1158_DATA_RATE_1821; // in units of Samples Per Second
    ads1158CurrentConfigs.config1.sbcs   = ADS1158_SENSOR_BIAS_24;
    ads1158CurrentConfigs.config1.dly    = ADS1158_CONVERTER_DELAY_48;
    ads1158CurrentConfigs.config1.idlmod = ADS1158_IDLE_MODE_STANDBY;

    /* Set up the muxsch register to our specific settings */
    // This whole register is a don't care since we are auto-scan mode

    /* Set up the muxdif, muxsg0, muxsg1, sysred registers to our specific settings */
    ads1158_defaultEnabledChannels();
    ads1158_setConfigChannelRegisters();

    /* Set up the gpioc and gpiod registers to our specific settings */
    ads1158_defaultGPIOConfig();

    ads1158ConfigChanged = true;
}

void ads1158_setConfigChannelRegisters()
{
    /* Set up the muxdif register to our specific settings */
    ads1158CurrentConfigs.muxdif.diff0 = ads1158EnabledChannels[ADS1158_CHANNEL_ID_DIFF0];
    ads1158CurrentConfigs.muxdif.diff1 = ads1158EnabledChannels[ADS1158_CHANNEL_ID_DIFF1];
    ads1158CurrentConfigs.muxdif.diff2 = ads1158EnabledChannels[ADS1158_CHANNEL_ID_DIFF2];
    ads1158CurrentConfigs.muxdif.diff3 = ads1158EnabledChannels[ADS1158_CHANNEL_ID_DIFF3];
    ads1158CurrentConfigs.muxdif.diff4 = ads1158EnabledChannels[ADS1158_CHANNEL_ID_DIFF4];
    ads1158CurrentConfigs.muxdif.diff5 = ads1158EnabledChannels[ADS1158_CHANNEL_ID_DIFF5];
    ads1158CurrentConfigs.muxdif.diff6 = ads1158EnabledChannels[ADS1158_CHANNEL_ID_DIFF6];
    ads1158CurrentConfigs.muxdif.diff7 = ads1158EnabledChannels[ADS1158_CHANNEL_ID_DIFF7];

    /* Set up the muxsg0 register to our specific settings */
    ads1158CurrentConfigs.muxsg0.ain0 = ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN0];
    ads1158CurrentConfigs.muxsg0.ain1 = ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN1];
    ads1158CurrentConfigs.muxsg0.ain2 = ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN2];
    ads1158CurrentConfigs.muxsg0.ain3 = ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN3];
    ads1158CurrentConfigs.muxsg0.ain4 = ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN4];
    ads1158CurrentConfigs.muxsg0.ain5 = ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN5];
    ads1158CurrentConfigs.muxsg0.ain6 = ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN6];
    ads1158CurrentConfigs.muxsg0.ain7 = ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN7];

    /* Set up the muxsg1 register to our specific settings */
    ads1158CurrentConfigs.muxsg1.ain8  = ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN8];
    ads1158CurrentConfigs.muxsg1.ain9  = ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN9];
    ads1158CurrentConfigs.muxsg1.ain10 = ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN10];
    ads1158CurrentConfigs.muxsg1.ain11 = ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN11];
    ads1158CurrentConfigs.muxsg1.ain12 = ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN12];
    ads1158CurrentConfigs.muxsg1.ain13 = ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN13];
    ads1158CurrentConfigs.muxsg1.ain14 = ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN14];
    ads1158CurrentConfigs.muxsg1.ain15 = ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN15];

    /* Set up the sysred register to our specific settings */
    ads1158CurrentConfigs.sysred.offset = ads1158EnabledChannels[ADS1158_CHANNEL_ID_OFFSET];
    ads1158CurrentConfigs.sysred.vcc    = ads1158EnabledChannels[ADS1158_CHANNEL_ID_VCC];
    ads1158CurrentConfigs.sysred.temp   = ads1158EnabledChannels[ADS1158_CHANNEL_ID_TEMP];
    ads1158CurrentConfigs.sysred.gain   = ads1158EnabledChannels[ADS1158_CHANNEL_ID_GAIN];
    ads1158CurrentConfigs.sysred.ref    = ads1158EnabledChannels[ADS1158_CHANNEL_ID_REF];
}


void io_ads1158_enableChannel(ads1158_channel_E channel)
{
    ads1158EnabledChannels[channel]  = ADS1158_CHANNEL_ENABLED;
    ads1158_countEnabledChannels();
    ads1158_setConfigChannelRegisters();
    ads1158ConfigChanged = true;
}

void io_ads1158_disableChannel(ads1158_channel_E channel)
{
    ads1158EnabledChannels[channel]  = ADS1158_CHANNEL_DISABLED;
    ads1158_countEnabledChannels();
    ads1158_setConfigChannelRegisters();
    ads1158ConfigChanged = true;
}

void ads1158_setPrecision(ads1158_read_precision_E precision)
{
    ads1158readPrecision = precision;
    if (ads1158readPrecision == ADS1158_READ_PRECISION_MICROVOLTS)
    {
        //disable all channels
        ads1158_defaultEnabledChannels();
        ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN0] = ADS1158_CHANNEL_ENABLED;
        
        ads1158NumEnabledChannels = 1;
    }
    else if (ads1158readPrecision == ADS1158_READ_PRECISION_MILLIVOLTS)
    {
        //disable all channels
        ads1158_defaultEnabledChannels();
        ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN1] = ADS1158_CHANNEL_ENABLED;
        ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN2] = ADS1158_CHANNEL_ENABLED;
        ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN3] = ADS1158_CHANNEL_ENABLED;
        ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN4] = ADS1158_CHANNEL_ENABLED;
        ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN5] = ADS1158_CHANNEL_ENABLED;
        ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN6] = ADS1158_CHANNEL_ENABLED;
        ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN7] = ADS1158_CHANNEL_ENABLED;
        ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN8] = ADS1158_CHANNEL_ENABLED;
        ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN9] = ADS1158_CHANNEL_ENABLED;
        ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN10] = ADS1158_CHANNEL_ENABLED;
        ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN11] = ADS1158_CHANNEL_ENABLED;
        ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN12] = ADS1158_CHANNEL_ENABLED;
        ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN13] = ADS1158_CHANNEL_ENABLED;
        ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN14] = ADS1158_CHANNEL_ENABLED;
        ads1158EnabledChannels[ADS1158_CHANNEL_ID_AIN15] = ADS1158_CHANNEL_ENABLED;

        ads1158NumEnabledChannels = 15;
    }

    ads1158_setConfigChannelRegisters();
    ads1158ConfigChanged = true;
}

void io_ads1158_changePrecision(ads1158_read_precision_E precision)
{
    if (ads1158readPrecision != precision)
    {
       ads1158_setPrecision(precision);
    }
}


void io_ads1158_setGPIODirection(ads1158_GPIO_E GPIO, ads1158_GPIO_direction_E direction)
{
    switch(GPIO)
    {
        case ADS1158_GPIO_0:
            ads1158CurrentConfigs.GPIOc.cio0 = direction;
            break;

        case ADS1158_GPIO_1:
            ads1158CurrentConfigs.GPIOc.cio1 = direction;
            break;

        case ADS1158_GPIO_2:
            ads1158CurrentConfigs.GPIOc.cio2 = direction;
            break;

        case ADS1158_GPIO_3:
            ads1158CurrentConfigs.GPIOc.cio3 = direction;
            break;

        case ADS1158_GPIO_4:
            ads1158CurrentConfigs.GPIOc.cio4 = direction;
            break;

        case ADS1158_GPIO_5:
            ads1158CurrentConfigs.GPIOc.cio5 = direction;
            break;

        case ADS1158_GPIO_6:
            ads1158CurrentConfigs.GPIOc.cio6 = direction;
            break;

        case ADS1158_GPIO_7:
            ads1158CurrentConfigs.GPIOc.cio7 = direction;
            break;

        default:
            //do nothing
            break;
    }

    ads1158ConfigChanged = true;
}

void io_ads1158_GPIOwrite(ads1158_GPIO_E GPIO, ads1158_GPIO_value_E value)
{
    switch(GPIO)
    {
        case ADS1158_GPIO_0:
            ads1158CurrentConfigs.GPIOd.dio0 = value;
            break;

        case ADS1158_GPIO_1:
            ads1158CurrentConfigs.GPIOd.dio1 = value;
            break;

        case ADS1158_GPIO_2:
            ads1158CurrentConfigs.GPIOd.dio2 = value;
            break;

        case ADS1158_GPIO_3:
            ads1158CurrentConfigs.GPIOd.dio3 = value;
            break;

        case ADS1158_GPIO_4:
            ads1158CurrentConfigs.GPIOd.dio4 = value;
            break;

        case ADS1158_GPIO_5:
            ads1158CurrentConfigs.GPIOd.dio5 = value;
            break;

        case ADS1158_GPIO_6:
            ads1158CurrentConfigs.GPIOd.dio6 = value;
            break;

        case ADS1158_GPIO_7:
            ads1158CurrentConfigs.GPIOd.dio7 = value;
            break;

        default:
            //do nothing
            break;
    }

    ads1158ConfigChanged = true;
}

ads1158_GPIO_value_E io_ads1158_GPIOread(ads1158_GPIO_E GPIO)
{
    //TODO: actually do something
    return ADS1158_GPIO_VALUE_LOW;
}

uint16_t * io_ads1158_getMilliVoltChannels()
{
    return ads1158CurrentMilliVoltageValues;
}

uint32_t io_ads1158_getMicroVoltChannel()
{
    return ads1158CurrentMicroVoltageValue;
}


//void io_ads1158_getAllChannelVoltages(io_ads1158_voltage_T * channelVoltages)
//{    
//    memcpy(channelVoltages, ads1158CurrentMilliVoltageValues, (ADS1158_NUM_CHANNEL_IDS * sizeof(io_ads1158_voltage_T))); 
//}