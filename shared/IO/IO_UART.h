/*
 * File:    IO_UART.h
 * Created: 01/10/2017
 *
 * Find out about how a timeout would interact with task scheduler.
*/

#ifndef IO_UART_H
#define IO_UART_H

#include "EVT_std_includes.h"
/* ========================================================================
 * Public Enumerated Types Definitions
 * ========================================================================
*/

/* These enums correspond to the mc_uart_baud_E enums */
/* Do not change these without changing the mc level enums */
typedef enum io_uart_baud_E
{
    IO_UART_BAUD_9600        = 1,
    IO_UART_BAUD_19200       = 2,
    IO_UART_BAUD_38400       = 3,
    IO_UART_BAUD_57600       = 4,
    IO_UART_BAUD_115200      = 5
} io_uart_baud_E;

/* These enums correspond to the mc_uart_channel_E enums*/
/* Do not change these without changing the mc level enums */
typedef enum io_uart_channel_E
{
    IO_UART_CHANNEL1         = 1,
    IO_UART_CHANNEL2         = 2,
    IO_UART_NUM_CHANNELS     = 2,
} io_uart_channel_E;

typedef enum io_uart_status_E
{
    IO_UART_OFF         = 0,
    IO_UART_GOOD        = 1,
    IO_UART_DATA_LOST   = 2,
    IO_UART_FRAME_ERROR = 3,
} io_uart_status_E;

/* ========================================================================
 * Public Function Declarations
 * ========================================================================
*/

/* Opens a Serial communication port. 
 *
 * Arguments:
 * 
 * Notes:
 *  - should primarily be called by the task manager, others should
 *      use io_uart_reopen
 */
void io_uart_init();

/* Closes the current UART channel and opens a new one with a new
 *  baud rate.
 *
 * Arguments:
 *  channel - The new UART channel to open.
 *  baud - The new baud rate to use.
 *
 * Notes:
 */
void io_uart_reopen(io_uart_baud_E baud);

/* Closes the current UART port.
 *
 * Notes:
 */
void io_uart_close();

/* Handles the UART transfers.
 * 
 * Notes:
 *  - Reads data into the receive buffer
 *  - Prints data from the transmit buffer
 *  - Updates the status if errors occur
 */
void io_uart_process();

/* Reads an array of 'num' bytes from  the UART rx buffer 
 *
 * Returns:
 *  - The number of bytes read from the buffer.
 * 
 * Arguments:
 *  - data: An array to store the data read.
 *  - num: The desired number of bytes to read.
 */
uint8_t io_uart_readN(uint8_t *data, uint8_t num);

/* Writes an array of 'num' bytes to the UART tx buffer 
 *
 * Returns:
 *  - The number of bytes written to the buffer.
 * 
 * Arguments:
 *  - data: An array of 8-bit values to write.
 *  - num: The desired number of bytes to write.
 */
uint8_t io_uart_writeN(uint8_t *data, uint8_t num);

/* Writes a hex number to the UART port as a string.
 * Prints in a "0x----" format.
 *
 * Returns:
 *  - 0 on success, -1 on failure.
 *
 * Arguments:
 *  - data: The number to write.
 */
int8_t io_uart_printHex(uint16_t data);

/* Returns the current status of the UART module.
 *
 * Returns:
 *  - An enum representing the current state.
 */
io_uart_status_E io_uart_getStatus();

#endif /* IO_UART_H */
