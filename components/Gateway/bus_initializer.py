
import socket

from device import Device
from message_constants import *

def bus_initialization_sequence(can, header, message_factory):
    """
    https://wiki.rit.edu/display/EVT/CAN+Bus#CANBus-Exampleflow:
    """

    # Request name and firmware version of devices on bus.
    devices = request_name_firmware_version(can, header, message_factory)

    # Request name and units of data of devices on bus.
    request_data_types(can, header, devices, message_factory)

    # Tell devices they can start transmitting.
    request_transmit_data(can, header, message_factory)

    return devices

def request_name_firmware_version(can, header, message_factory):
    """
    Sends a request for device names and firmware versions and waits for the
    responses from every device on the bus.

    Returns dict of device objects.
    """

    devices = {}

    message_id = header.get_message_id(REQUEST_NAME_FIRMWARE_VERSION_MSG)
    version_id = header.get_message_id(FIRMWARE_VERSION_MSG)
    name_id = header.get_message_id(NAME_MSG)

    request = message_factory.create_message(message_id)
    can.send(request)

    try:
        while True:
            message = can.receive()

            device_id = message.device_id

            if message.message_id == name_id:
                devices[device_id] = Device(device_id, message.data)
            elif message.message_id == version_id:
                devices[device_id].set_version(message.data)

    except socket.timeout:
        # Break out of while loop on timeout, no more devices sending on bus.
        pass

    # TODO error checking on unequal length of both dicts
    # thoughts: name required, version isn't. special request to device missing info.
    # if len(device_names) != len(device_versions):
    #     log_error()

    return devices

def request_data_types(can, header, devices, message_factory):
    """
    Sends a request for device data types and waits for the responses from every
    device on the bus. Data types include name and unit for each position.

    Updates the devices dictionary.
    """

    # Send the request
    message_id = header.get_message_id(REQUEST_DEVICE_DATA_TYPES_MSG)
    data_type_id = header.get_message_id(DATA_TYPE_MSG)

    request = message_factory.create_message(message_id)
    can.send(request)

    try:
        # Wait for a response from all devices
        while True:
            message = can.receive()

            if message.message_id != data_type_id:
                continue

            # 1st byte is position, 2nd is name (1) or unit (0), 3-8 are value.
            data_bytes = message.data
            position = data_bytes[0]
            name_vs_unit = data_bytes[1]
            value = data_bytes[2:]

            # True for name, false for unit.
            # Name should always come before the corresponding unit.
            if name_vs_unit:
                devices[message.device_id].add_data_name(position, value)
            else:
                devices[message.device_id].add_data_unit(position, value)

    except socket.timeout:
        # Break out of while loop on timeout, no more devices sending on bus.
        pass

    return

def request_transmit_data(can, header, message_factory):
    """
    Send a message telling all devices to begin transmitting their data.
    """
    message_id = header.get_message_id(REQUEST_TRANSMIT_DATA_MSG)
    request = message_factory.create_message(message_id)
    can.send(request)
