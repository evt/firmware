
import struct

from collections import defaultdict

class Device():
    """
    A device found on the CAN bus during the bus initialization sequence.
    """

    def __init__(self, device_id, name):
        self.id = device_id
        self.name = name.decode('ascii')
        self.version = None
        self.data_types = defaultdict(dict)

    def set_version(self, version):
        self.version = version

    def add_data_name(self, position, name):
        self.data_types[position]['name'] = name.decode('ascii')

    def add_data_unit(self, position, unit):
        self.data_types[position]['unit'] = unit.decode('ascii')

    def set_data_value(self, position, value):
        # >H is Big endian unsigned short
        number = struct.unpack('>H', value)
        self.data_types[position]['value'] = number[0]

    def get_data_name(position):
        return self.data_types[position].name

    def clear_values():
        for _, data_type in self.data_types.items():
            del data_type.value

    def __str__(self):
        return "Device: ID=%s, Name=%s, Version=%s, Data Types=%s" % (
            self.id, self.name, self.version, dict(self.data_types)
        )
