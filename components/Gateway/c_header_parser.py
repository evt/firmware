#!/usr/bin/env python3

import sys
import re

from collections import defaultdict

from message_constants import DS_DATA_START_MSG, DS_DATA_END_MSG

# Regex pattern to find the end of Device ID section
END_DEV_ID_PATTERN = "^//end DEVIDs"

# Regex pattern to find #define macros
DEFINE_MACRO_PATTERN = "^#define\s+([A-Z0-9_]+)\s+([a-zA-Z0-9_]+)"

DEVICE_CLASSES = ["DS", "GTW"]
DEFAULT_DEVICE_CLASS = "DS"
DS_DATA_PREFIX = "DS_DATA_"

class CHeader:

    def __init__(self):
        self.reset()

    def parse(self, fname):
        self.reset()

        # Start with Device IDs
        dev_id_section = True

        with open(fname, "r") as f:
            for line in f:
                # Check if Device ID section is over and switch to Message IDs
                end_dev_line = re.search(END_DEV_ID_PATTERN, line)
                if end_dev_line:
                    dev_id_section = False
                    continue

                # Check line for #define
                match = re.search(DEFINE_MACRO_PATTERN, line)
                if match:
                    _, name, value = match.group().split()
                    if dev_id_section:
                        self.dev_name_to_id[name] = int(value, 16);
                    else:
                        self.msg_name_to_id[name] = int(value, 16);

        self.dev_id_to_name = {v: k for k, v in self.dev_name_to_id.items()}
        self.build_message_id_to_name_maps()

        self.ds_data_start = self.get_message_id(DS_DATA_START_MSG)
        self.ds_data_end = self.get_message_id(DS_DATA_END_MSG)

    def get_device_id(self, name):
        return self.dev_name_to_id[name]

    def get_device_name(self, number):
        return self.dev_id_to_name[number]

    def get_message_id(self, name):
        return self.msg_name_to_id[name]

    def get_message_name(self, message_id, device_id=None):
        device_class = self.get_device_class(device_id)
        message_name = ""

        # Special case for DS_DATA range
        if device_class == "DS" and (
            message_id > self.ds_data_start and message_id < self.ds_data_end):
            data_number = message_id - self.ds_data_start
            message_name = DS_DATA_PREFIX + str(data_number)
        else:
            message_name = self.msg_id_to_name[device_class][message_id]

        return message_name

    def reset(self):
        self.dev_name_to_id = {}
        self.msg_name_to_id = {}
        self.dev_id_to_name = {}
        self.msg_id_to_name = defaultdict(dict)
        self.ds_data_start = -1
        self.ds_data_end = -1

    def get_device_class(self, device_id):
        device_class = DEFAULT_DEVICE_CLASS
        if device_id:
            device_name = self.get_device_name(device_id)
            if device_name in DEVICE_CLASSES:
                device_class = device_name

        return device_class

    def build_message_id_to_name_maps(self):
        self.msg_id_to_name = defaultdict(dict)
        for name, message_id in self.msg_name_to_id.items():
            device_class = name.split("_")[0]
            self.msg_id_to_name[device_class][message_id] = name


def main():
    header_filename = None
    if len(sys.argv) == 2:
        header_filename = sys.argv[1]
    else:
        import message_constants
        header_filename = message_constants.CAN_MESSAGES_HEADER_PATH
        print('Header file not provided, default is %s' % header_filename)

    parser = CHeader()
    parser.parse(header_filename)

    import pprint
    pprint.pprint(parser.dev_name_to_id)
    pprint.pprint(parser.msg_name_to_id)
    pprint.pprint(parser.dev_id_to_name)
    pprint.pprint(dict(parser.msg_id_to_name))

if __name__ == "__main__":
    sys.exit(main())
