
def convert_to_bytes(data):
    """
    Ensure data is of type bytes.
    """
    result = None
    if isinstance(data, bytes):
        result = data
    elif isinstance(data, str):
        result = str.encode(data)
    elif isinstance(data, int):
        import struct
        result = struct.pack('>I', data)

    return result
