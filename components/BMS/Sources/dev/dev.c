/* 
 * File:   dev.c
 * Author: Derek Gutheil <dkg8689@rit.edu>
 *
 * Created on September 8, 2016, 8:07 PM
 */

#include "dev.h"

void dev_init()
{
    //Initialize the dev layer
}

void dev_1ms()
{
    //Run any dev specific 1ms cyclic tasks
}

void dev_10ms()
{
    //Run any dev specific 10ms cyclic tasks
}

void dev_100ms()
{
    //Run any dev specific 100ms cyclic tasks
}