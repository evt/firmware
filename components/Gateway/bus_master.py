
import bus_initializer
import can_message
import io_can

from collections import defaultdict

from message_constants import DS_DATA_START_MSG

BMS_PREFIX = "BMS"

class BusMaster:
    """
    Manages the CAN bus and all communications across it.
    """

    def __init__(self, id, header, logger = None):
        self.id = id
        self.header = header
        self.logger = logger

        self.can = io_can.IOCan()
        self.message_factory = can_message.MessageFactory(id)

    def init_bus(self):
        self.devices = bus_initializer.bus_initialization_sequence(
            self.can,
            self.header,
            self.message_factory
        )

        for _, device in self.devices.items():
            print(device)

    def listen(self):
        self.can.set_timeout(None)

        ds_data_start = self.header.get_message_id(DS_DATA_START_MSG)

        # BMS specific logger
        device_message_count = defaultdict(int)
        import datetime
        for id, device in self.devices.items():
            bms_file = open(device.name + "_log.csv", "w")
            header_list = ["timestamp"]
            for position, data_type in device.data_types.items():
                header_list.append("%s (%s)" % (
                    data_type['name'], data_type['unit']
                ))

            header = ",".join(header_list)
            bms_file.write(header + "\n")
            bms_file.flush()
            bms_file.close()

        while True:
            message = self.can.receive()

            device_name = self.header.get_device_name(message.device_id)
            message_name = self.header.get_message_name(message.message_id, message.device_id)
            line = "%s,%s,%s" % (device_name, message_name, message.data)
            print(line)
            self.logger.log_message(line)

            # Due to loopback, we receive our own messages which breaks things
            if message.device_id == self.id:
                continue
            device = self.devices[message.device_id]

            # Handle BMS messages separately so we can log their positions together
            if device.name.startswith(BMS_PREFIX):
                position = message.message_id - ds_data_start + 1 # add 1 because cells come in 1 to 12
                # TODO use position as index into sorted key list
                if position < 0:
                    continue # this wasn't a data message

                device.set_data_value(position, message.data)
                device_message_count[device.name] += 1

                # If all positions have been filled, flush values
                if device_message_count[device.name] == len(device.data_types):
                    device_message_count[device.name] = 0
                    message_list = []
                    timestamp = str(datetime.datetime.now())
                    message_list.append(timestamp)
                    for position, data_type in device.data_types.items():
                        message_list.append(str(data_type['value']))

                    line = ",".join(message_list)
                    print(line)
                    bms_file = open(device.name + "_log.csv", "a")
                    bms_file.write(line + "\n")
                    bms_file.close()
