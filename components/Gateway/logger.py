import abc
import datetime
import queue
import shutil
import socket
import sys

class Logger:
	"""
	Interface for a logger class. Provides one method called log_message() that
	stores a CAN message. A logger only handles format and storage location.
	"""
	__metaclass__ = abc.ABCMeta

	def __init__(self):
		self.messages = queue.Queue()

	@abc.abstractmethod
	def log_message(self, message):
		pass

class LocalLogger(Logger):
	"""
	Implementation of Logger that stores messages on disk locally.
	"""

	def __init__(self, filename):
		self.f = open(filename, "w")
		super().__init__()

	def log_message(self, message):
		timestamp = str(datetime.datetime.now())
		line = timestamp + "," + message
		self.f.write(line + "\n")


class NetworkLogger(Logger):
	"""
	Implementation of Logger that sends messages to a server.
	"""

	def __init__(self):
		super().__init__()

	def log_message(self, message):
		pass
