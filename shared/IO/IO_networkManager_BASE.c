/*
 * File: IO_networkManager.c
 * Created: 4/1/2017
 *
 * Responsible for collecting data from other applications and packaging the
 * data into formatted frames for transmission on the CAN bus.
*/

#include "IO_networkManager.h"
#include "IO_CAN.h"

/* ============================================================================
 * Enumerated Type Definitions
 * ============================================================================
*/


/* ============================================================================
 * Private Module Variables
 * ============================================================================
*/


/* ============================================================================
 * Private Module Function Declarations
 * ============================================================================
*/
/* GENERATED DECLARATIONS */

/* ============================================================================
 * Module Function Definitions
 * ============================================================================
*/

/* Initializes the network manager IO layer. */
void io_networkManager_init()
{
    return;
}


/* Instructs the network manager to package a CAN data frame formatted based on
 * a CAN ID with data collected from an application and send it. */
void io_networkManager_processFrame(uint16_t id, void * data)
{
    /* Each case in this switch statement represents a different CAN Message as
     * it is defined in the DBC file. Each case runs a unique function
     * generated for that message to package the bits as defined by that
     * message and sends it to IO_CAN. */
    io_can_message_S message;

    switch (id)
    {
        /* CASE STATEMENT REPLACEMENT */
        default:
        {
            /* Error. */
        }
    }

    io_can_transmitMessage(&message);

    return;
}

/* GENERATED FUNCTION DEFINITIONS */