/*
 * File:   bootloader.c
 * Author: James Moretti
 */

#include "bootloader.h"
#include "MC_register_defaults.h"

// Offset application interrupt entry points. Ignore these warnings
#asm
PSECT intcode
    goto    PROGRAM_START+0x8
PSECT intcodelo
    goto    PROGRAM_START+0x18
#endasm


void main() {
    // Do our bootloader stuff
    TRISB1 = 0; //initialize light
    bootload();
    
    // Go to program start. I have NO idea if this actually works yet.
    asm("GOTO " ___mkstr(PROGRAM_START));
}

void bootload() {
    blinkHello();

//    // Initialize communication protocol
//    serialInit();
//    while(!DataRdy1USART()); // Wait for data on serial
//
//    // Check for updates
//    char c = Read1USART();
//    if(c == UPDATE_AVAILABLE_FLAG) {
//        Write1USART(UPDATE_ACCEPT_FLAG); // Tell PC to send updates
//
//        // Write recieved data to program storage
//        unsigned long memaddr;
//        for(memaddr = PROGRAM_START; (c = Read1USART()) != '\0'; memaddr++) {
//            WriteBytesFlash(memaddr, 1, &c);
//        }
//
//        //TODO: Calculate program checksum and compare with recieved checksum
//
//    }

    blinkHello();
    blinkHello();
}

void blinkHello() {
    LATBbits.LATB1 = 1;
    __delay_ms(30);
    __delay_ms(30);
    __delay_ms(30);
    LATBbits.LATB1 = 0;
    __delay_ms(30);
    __delay_ms(30);
    __delay_ms(30);
}