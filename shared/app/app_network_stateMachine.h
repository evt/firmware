/* 
 * File:   app_network_stateMachine.h
 * Author: Derek Gutheil <dkg8689@rit.edu>
 *
 * Created on October 8, 2016
 */

#ifndef APP_NETWORK_STATEMACHINE_H
#define APP_NETWORK_STATEMACHINE_H

#include <EVT_std_includes.h>

/* ===========================================================================================================================
 * Public Enumerated type definitions
 * ===========================================================================================================================
*/
typedef enum app_network_sm_state_E
{
	APP_NETWORK_SM_STATE_INIT          = 0,
	APP_NETWORK_SM_STATE_ISOLATED      = 1,
	APP_NETWORK_SM_STATE_NEGOTIATIONS  = 2,
	APP_NETWORK_SM_STATE_NETWORKED     = 3,
	APP_NETWORK_SM_STATE_QUIET         = 4,
	APP_NETWORK_SM_STATE_NUM_STATES
} app_network_sm_state_E;

typedef enum app_network_sm_transition_E //Available transitions
{
    APP_NETWORK_SM_TRANSITION_NONE             = 0,
    APP_NETWORK_SM_TRANSITION_INIT_SUCCESS     = 1, //Completed initialization routines
    APP_NETWORK_SM_TRANSITION_GTW_CONT         = 2, //GTW establishes communications and is in the process or establishing data structures to be transfered
    APP_NETWORK_SM_TRANSITION_TIME_OUT         = 3, //Some time period has passed with silence from the GTW
    APP_NETWORK_SM_TRANSITION_GTW_TRANS        = 4, //The GTW has issued a "Begin Transmitting" Message
    APP_NETWORK_SM_TRANSITION_GTW_QUIET        = 5, //The GTW has issued a "CAN Quiet" Message
} app_network_sm_transition_E;


/* ===========================================================================================================================
 * Public Structure definitions
 * ===========================================================================================================================
*/



/* ===========================================================================================================================
 * Public function declarations
 * ===========================================================================================================================
*/

/*
 * Runs the application layer network module state machine (processes states and transitions)
 * Gets called by a periodic function (100ms) in app.c (may change to app_network.c)
 * This is the primary entry point for running the network module state machine
*/
void app_network_sm();

/*
 * Returns the current state of the state machine
*/
app_network_sm_state_E app_network_sm_getState();

/*
 * Returns the current transition reason of the state machine
*/
app_network_sm_transition_E app_network_sm_getTransition();

#endif    /* APP_NETWORK_STATEMACHINE_H */