#include "app_current.h"
#include "IO_ADS1158.h"

static int16_t current_amps;

void app_current_calcCurrent()
{
    uint32_t uVolts;

    io_ads1158_changePrecision(ADS1158_READ_PRECISION_MICROVOLTS);
    
    uVolts = io_ads1158_getMicroVoltChannel();
    current_amps = ((int16_t) (uVolts/667)) - 3750;
}

int16_t app_current_getCurrent()
{
    return current_amps;
}
