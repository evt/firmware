#!/usr/bin/env python3

import encoder
import socket
import struct
import sys

from can_message import CANMessage

# Hardware CAN interface defaults to can0
DEFAULT_CAN_IFACE = "can0"

# Default buffer size for receiving a message
DEFAULT_RECV_BUFSIZE = 16

# Default timeout is one second, making it non-blocking
DEFAULT_SOCKET_TIMEOUT = 1

# CAN frame packing/unpacking (see `struct can_frame` in <linux/can.h>)
# https://docs.python.org/3/library/struct.html#format-characters
CAN_FRAME_FMT = "=IB3x8s"

class IOCan:
    """
    Abstracts away the IO layer of CAN on the BeagleBone Black.
    Currently this only supports the standard frame format (11 bit ID).
    """

    def __init__(self,
        iface = DEFAULT_CAN_IFACE,
        sock = None,
        timeout = DEFAULT_SOCKET_TIMEOUT,
    ):
        """
        Create a raw socket and bind it to the given CAN interface.
        """
        if not sock:
            self.sock = socket.socket(socket.AF_CAN, socket.SOCK_RAW, socket.CAN_RAW)
            self.sock.bind((iface,))
        else:
            self.sock = sock

        self.sock.settimeout(timeout)

    def send(self, message):
        """
        Construct a CAN frame out of the id and data and send it.
        """
        #TODO ensure that all data has been sent and redeliver if not
        # (see https://docs.python.org/2/howto/sockets.html#socket-howto)
        # or use sendall but that doesn't let us know how much was sent on error
        can_frame = build_can_frame(message.can_id, message.data)
        bytes_sent = self.sock.send(can_frame)

    def receive(self,
        bufsize = DEFAULT_RECV_BUFSIZE,
        timeout = None,
    ):
        """
        Block execution until a message is received over the CAN interface. This
        message is then dissected into its id, data length, and data, and
        returned as a message object.

        If the socket's timeout is set then it won't block longer than that. An
        optional timeout can be given that will only apply to a single call.
        """

        original_timeout = self.get_timeout()
        if timeout and timeout != original_timeout:
            self.set_timeout(timeout)

        can_frame = self.sock.recv(bufsize)

        self.set_timeout(original_timeout)
        # TODO test for can frame receiving nothing, meaning other end closed
        # TODO wrap socket timeout with a custom one
        # that also includes receiving nothing; maybe called receive_failed
        can_id, can_dlc, data = dissect_can_frame(can_frame)
        message = CANMessage(can_id, data)

        return message

    def set_timeout(self, timeout):
        """
        Time in seconds. Zero is non-blocking, None is permanently blocking.
        """
        self.sock.settimeout(timeout)

    def get_timeout(self):
        return self.sock.gettimeout()


def build_can_frame(can_id, data):
    """
    Pack strings into C struct binary data.
    """
    data_bytes = encoder.convert_to_bytes(data)
    can_dlc = len(data_bytes)
    return struct.pack(CAN_FRAME_FMT, can_id, can_dlc, data_bytes)

def dissect_can_frame(frame):
    """
    Unpack C struct binary data into strings.
    """
    can_id, can_dlc, data = struct.unpack(CAN_FRAME_FMT, frame)
    return (can_id, can_dlc, data[:can_dlc])


def main():
    """
    Sample program to test send and receive.
    """
    can_interface = None
    if len(sys.argv) == 2:
        can_interface = sys.argv[1]
    else:
        can_interface =  DEFAULT_CAN_IFACE
        print('CAN device name (can0, slcan0, etc.) not provided, default is %s' %
            DEFAULT_CAN_IFACE)

    can = IOCan(can_interface, timeout = None)

    while True:
        print('Waiting to receive CAN frame')
        cf = can.sock.recv(16)
        print('CAN frame: %s' % cf)
        can_id, can_dlc, data = dissect_can_frame(cf)
        print('can id: %s; hex: %s' % (can_id, hex(can_id)))
        devId = can_id
        devId = devId >> 7
        msgId = can_id & int(127)
        print('device id: %s; hex: %s' % (devId, hex(devId)))
        print('message id: %s; hex: %s' % (msgId, hex(msgId)))
        print('data length: %s; hex: %s' % (can_dlc, hex(can_dlc)))
        print('data: %s; decimal: %s' % (data, list(data)))

        try:
            print('Forwarding received CAN frame')
            can.sock.send(cf)
        except socket.error:
            print('Error forwarding CAN frame')

        try:
            print("Sending new CAN frame")
            can.sock.send(build_can_frame(0x01, b'F\x02\x03\x04\x05'))
        except socket.error:
            print('Error sending CAN frame')


if __name__ == "__main__":
    sys.exit(main())
