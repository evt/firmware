#!/usr/bin/env python3

"""
A mock CAN device to test the Gateway while connected to loopback. It responds
to the Gateway's initialization sequence then continuously transmits data.

To test, firsts run this, then run the main Gateway file.
"""

import random
import os
import sys
import time

PARENT_FOLDER = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    "..",
)
sys.path.append(PARENT_FOLDER)

import io_can
import c_header_parser
import can_message

from message_constants import *

MOCK_DEVICE_ID = 0xF
MOCK_NAME = "BMS_MOCK"
MOCK_VERSION = "1.00"
MOCK_DATA_NAME = b'\x00\x01test0'
MOCK_DATA_UNIT = b'\x00\x00testmV'
DATA_MESSAGE_NAME = "DS_DATA_START"

def init_can():
    return io_can.IOCan(timeout = None)

def init_bus(can, header, message_factory):
    """
    https://wiki.rit.edu/display/EVT/CAN+Bus#CANBus-Exampleflow:
    """

    # Send name and firmware version of this device.
    send_name_firmware_version(can, header, message_factory)

    # Send name and units of data for this device.
    send_data_types(can, header, message_factory)

    # Continuously transmit data.
    transmit_data(can, header, message_factory)

def send_name_firmware_version(can, header, message_factory):
    """
    After receiving a request for the device name and firmware version, sends
    the name and version of this device.
    """

    request_message_id = header.get_message_id(REQUEST_NAME_FIRMWARE_VERSION_MSG)

    name_id = header.get_message_id(NAME_MSG)
    version_id = header.get_message_id(FIRMWARE_VERSION_MSG)

    while True:
        message = can.receive()

        if message.message_id != request_message_id:
            continue

        print("Sending name")
        name_message = message_factory.create_message(name_id, MOCK_NAME)
        can.send(name_message)
        print("Sending version")
        version_message = message_factory.create_message(version_id, MOCK_VERSION)
        can.send(version_message)
        return

def send_data_types(can, header, message_factory):
    """
    Sends data types after receiving the request for them.
    """

    request_message_id = header.get_message_id(REQUEST_DEVICE_DATA_TYPES_MSG)

    data_type_id = header.get_message_id(DATA_TYPE_MSG)

    while True:
        message = can.receive()

        if message.message_id != request_message_id:
            continue

        print("Sending data name")
        name_message = message_factory.create_message(data_type_id, MOCK_DATA_NAME)
        can.send(name_message)
        print("Sending data unit")
        unit_message = message_factory.create_message(data_type_id, MOCK_DATA_UNIT)
        can.send(unit_message)
        return

def transmit_data(can, header, message_factory):
    """
    Wait until there is a request to start transmitting data, then transmit
    random test data from this device continuously.
    """

    request_message_id = header.get_message_id(REQUEST_TRANSMIT_DATA_MSG)

    while True:
        message = can.receive()

        if message.message_id == request_message_id:
            break

    message_id = header.get_message_id(DATA_MESSAGE_NAME)

    while True:
        data = random.randint(0, 511)

        print("Sending data: %d" % data)
        message = message_factory.create_message(message_id, data)
        can.send(message)
        time.sleep(1) # 1 second

def main():
    header = c_header_parser.CHeader()
    header.parse(CAN_MESSAGES_HEADER_PATH)
    can = init_can()
    message_factory = can_message.MessageFactory(MOCK_DEVICE_ID)
    init_bus(can, header, message_factory)
    return 0

if __name__ == '__main__':
    main()
