/* 
 * File:   MC_pin.h
 * Author: Derek
 *
 * Created on December 8, 2016, 1:08 PM
 */

#ifndef MC_PIN_H
#define MC_PIN_H

#include "EVT_std_includes.h"

/* ===========================================================================================================================
 * Public precompiler definitions
 * ===========================================================================================================================
*/

/* ===========================================================================================================================
 * Public Enumerated type definitions
 * ===========================================================================================================================
*/

typedef enum MC_PIN_DIRECTION_E
{
    MC_PIN_OUTPUT           = 0U,
    MC_PIN_INPUT            = 1U,
    MC_PIN_NUM_DIRECTIONS,
    MC_PIN_DIRECTION_ERROR
} MC_PIN_DIRECTION_E;

typedef enum MC_PIN_STATES_E
{
    MC_PIN_LOW              = 0U,
    MC_PIN_HIGH             = 1U,
    MC_PIN_NUM_PIN_STATES,
    MC_PIN_STATE_ERROR
} MC_PIN_STATES_E;

typedef enum MC_PIN_FUNCTION_SELECT_E
{
    MC_PIN_DIGITAL          = 0U,
    MC_PIN_ANALOG           = 1U,
    MC_PIN_NUM_FUNCTION_SELECT,
    MC_PIN_FUNCTION_ERROR
} MC_PIN_FUNCTION_SELECT_E;

typedef enum MC_PIN_E
{
    MC_PIN_NULL     = 0,
#ifdef _18F25K80
    MC_PIN_1        = 1,
    MC_PIN_2        = 2,
    MC_PIN_3        = 3,
    MC_PIN_4        = 4,
    MC_PIN_5        = 5,
    MC_PIN_6        = 6,
    MC_PIN_7        = 7,
    MC_PIN_8        = 8,
    MC_PIN_9        = 9,
    MC_PIN_10       = 10,
    MC_PIN_11       = 11,
    MC_PIN_12       = 12,
    MC_PIN_13       = 13,
    MC_PIN_14       = 14,
    MC_PIN_15       = 15,
    MC_PIN_16       = 16,
    MC_PIN_17       = 17,
    MC_PIN_18       = 18,
    MC_PIN_19       = 19,
    MC_PIN_20       = 20,
    MC_PIN_21       = 21,
    MC_PIN_22       = 22,
    MC_PIN_23       = 23,
    MC_PIN_24       = 24,
    MC_PIN_25       = 25,
    MC_PIN_26       = 26,
    MC_PIN_27       = 27,
    MC_PIN_28       = 28,
    MC_PIN_NUM_PINS = 28, //This needs to be manually set to match the last pin, since valid pins start at 1 not 0
/* Map the ports to their pins */
    /*Port A*/
    MC_PIN_RA0      = MC_PIN_2,
    MC_PIN_RA1      = MC_PIN_3,
    MC_PIN_RA2      = MC_PIN_4,
    MC_PIN_RA3      = MC_PIN_5,
    MC_PIN_RA5      = MC_PIN_7,
    MC_PIN_RA6      = MC_PIN_10,
    MC_PIN_RA7      = MC_PIN_9,  
    /*Port B */
    MC_PIN_RB0      = MC_PIN_21,
    MC_PIN_RB1      = MC_PIN_22,
    MC_PIN_RB2      = MC_PIN_23,
    MC_PIN_RB3      = MC_PIN_24,
    MC_PIN_RB4      = MC_PIN_25,
    MC_PIN_RB5      = MC_PIN_26,
    MC_PIN_RB6      = MC_PIN_27,
    MC_PIN_RB7      = MC_PIN_28,
    /*Port C */
    MC_PIN_RC0      = MC_PIN_11,
    MC_PIN_RC1      = MC_PIN_12,
    MC_PIN_RC2      = MC_PIN_13,
    MC_PIN_RC3      = MC_PIN_14,
    MC_PIN_RC4      = MC_PIN_15,
    MC_PIN_RC5      = MC_PIN_16,
    MC_PIN_RC6      = MC_PIN_17,
    MC_PIN_RC7      = MC_PIN_18,
    /*Port E */
    MC_PIN_RE3      = MC_PIN_1,
#endif    /* PIC18F25K80 */
/* Map the tasks to their port. Should be the same for all similar processors */
    /*Analog inputs*/
    MC_PIN_AN0      = MC_PIN_RA0,
    MC_PIN_AN1      = MC_PIN_RA1,
    MC_PIN_AN2      = MC_PIN_RA2,
    MC_PIN_AN3      = MC_PIN_RA3,
    MC_PIN_AN4      = MC_PIN_RA5,
    MC_PIN_VREFM    = MC_PIN_RA2,
    MC_PIN_VREFP    = MC_PIN_RA3,
    /*CAN Ports */
    MC_PIN_CANTX      = MC_PIN_RB2,
    MC_PIN_CANRX      = MC_PIN_RB3,
    MC_PIN_CANTX_ALT  = MC_PIN_RC6,
    MC_PIN_CANRX_ALT  = MC_PIN_RC7,
    /*SPI Ports */
    MC_PIN_SS         = MC_PIN_RA5,
    MC_PIN_SCK        = MC_PIN_RC3,
    MC_PIN_SDI        = MC_PIN_RC4,
    MC_PIN_SDO        = MC_PIN_RC5,
    /*I2C Ports */
    MC_PIN_SCL        = MC_PIN_RC3,
    MC_PIN_SDA        = MC_PIN_RC4,
    /*UART Ports */
    MC_PIN_TX1        = MC_PIN_RC6,
    MC_PIN_CK1        = MC_PIN_RC6,
    MC_PIN_RX1        = MC_PIN_RC7,
    MC_PIN_DT1        = MC_PIN_RC7,
    MC_PIN_TX2        = MC_PIN_RB6,
    MC_PIN_CK2        = MC_PIN_RB6,
    MC_PIN_RX2        = MC_PIN_RB7,
    MC_PIN_DT2        = MC_PIN_RB7,
    /*In Circuit Debugger and Programmer Ports */
    MC_PIN_MCLR       = MC_PIN_RE3,
    MC_PIN_PGC        = MC_PIN_RB6,
    MC_PIN_PGD        = MC_PIN_RB7,
    /*Comparators */
    MC_PIN_C1INA      = MC_PIN_RB0,
    MC_PIN_C1INB      = MC_PIN_RB1,
    MC_PIN_C2INA      = MC_PIN_RB4,
    MC_PIN_C2INB      = MC_PIN_RA5,
    MC_PIN_CTMUI      = MC_PIN_RA5,
    MC_PIN_CVREF      = MC_PIN_RA0,
    /*Fault Inputs */
    MC_PIN_FLT0       = MC_PIN_RB0,
    /*External Interrupts */
    MC_PIN_INT0       = MC_PIN_RB0,
    MC_PIN_INT1       = MC_PIN_RB1,
    MC_PIN_INT2       = MC_PIN_RB2,
    MC_PIN_INT3       = MC_PIN_RB3,
    MC_PIN_KBI0       = MC_PIN_RB4,
    MC_PIN_KBI1       = MC_PIN_RB5,
    MC_PIN_KBI2       = MC_PIN_RB6,
    MC_PIN_KBI3       = MC_PIN_RB7,
    /*Enhanced PWM outputs */
    MC_PIN_P1A        = MC_PIN_RB4,
    MC_PIN_P1B        = MC_PIN_RB1,
    MC_PIN_P1C        = MC_PIN_RB2,
    /*Timers */
    MC_PIN_T0CKI      = MC_PIN_RB5,
    MC_PIN_T1CKI      = MC_PIN_RA5,
    MC_PIN_T3CKI      = MC_PIN_RB5,
    MC_PIN_T1G        = MC_PIN_RC2,
    MC_PIN_T3G        = MC_PIN_RB7,
    /*Oscillators & clocks */
    MC_PIN_OSC1       = MC_PIN_RA7,
    MC_PIN_CLKIN      = MC_PIN_RA7,
    MC_PIN_OSC2       = MC_PIN_RA6,
    MC_PIN_CLKOUT     = MC_PIN_RA6,
    MC_PIN_SOSCO      = MC_PIN_RC0,
    MC_PIN_SCLKI      = MC_PIN_RC0,
    MC_PIN_SOSCI      = MC_PIN_RC1,
    MC_PIN_REFO       = MC_PIN_RC3,
    /*CTMU pulse delay input */
    MC_PIN_CTDIN      = MC_PIN_RB1,
    /*Capture input/Compare output/PWM output */
    MC_PIN_ECCP1      = MC_PIN_RB4,
    MC_PIN_CCP2       = MC_PIN_RC2,
    MC_PIN_CCP3       = MC_PIN_RC6,
    MC_PIN_CCP4       = MC_PIN_RC7,
    MC_PIN_CCP5       = MC_PIN_RB5,
    /*Wake up inputs */
    MC_PIN_ULPWU      = MC_PIN_RA0,
    /*High/Low-Voltage Detect input */
    MC_PIN_HLVDIN     = MC_PIN_RA5,
    /*CTMU */
    MC_PIN_CTED1      = MC_PIN_RB2,
    MC_PIN_CTED2      = MC_PIN_RB3,
    MC_PIN_CTPLS      = MC_PIN_RB4,
    /*
    For returning errors related to pins
     */
    MC_PIN_ERROR
    
} MC_PIN_E;


/* ===========================================================================================================================
 * Public Structure definitions
 * ===========================================================================================================================
*/

/* ===========================================================================================================================
 * Public function declarations
 * ===========================================================================================================================
*/

/*
 * MC_pin_setDirection: Give this function a pin and a direction, and it sets the pin to that direction
 * If a pin given is multiplexed with analog (AN0, AN1, etc.) this clears the Analog select bit as well
 * Giving an invalid pin or direction will return an error
 * returns: 0    : Success
 *          !0   : Error     
 */
int8_t MC_pin_setDirection(MC_PIN_E pin, MC_PIN_DIRECTION_E direction);

/*
 * MC_pin_getDirection: Give this function a pin and it returns the current direction that that pin is set to
 * Giving an invalid pin will return an error
 * returns: direction  : The direction of the pin as a MC_PIN_DIRECTION_E type
 *          <0         : Error     
 */
MC_PIN_DIRECTION_E MC_pin_getDirection(MC_PIN_E pin);


/* Derek 12/12/2016: We will probably want a MC_pin_setANalog/Digital function(s) in this file that switches certain pins between GPIO and ADC/PWM */

#endif  /* MC_PIN_H */