
# 11 bit CAN ID field composed of 4 bit Device ID and 7 bit Message ID
CAN_ID_BITS = 11
DEVICE_ID_BITS = 4
MESSAGE_ID_BITS = 7

# Message ID mask, AND to erase all but least 7 bits 00001111111
MESSAGE_ID_MASK = 127

class CANMessage():
    """
    Represents a CAN message.
    """

    def __init__(self, can_id, data):
        self.can_id = can_id
        self.data = data
        self.device_id, self.message_id = dissect_can_id(can_id)


class MessageFactory():
    """
    Factory that creates CAN messages. Simplifies ID creation.
    """

    def __init__(self, device_id):
        self.device_id = device_id

    def create_message(self, message_id, data = 0):
        can_id = build_can_id(self.device_id, message_id)
        return CANMessage(can_id, data)


def build_can_id(dev_id, msg_id):
    """
    Merges a 4 bit device ID with the 7 bit message ID to create an 11 bit ID.
    Message bits are in format: DDDDMMMMMMM

    Works by shifting the device bits into place then bit OR the message bits.

    Returns the 11 bit can ID.
    """
    can_id = dev_id << MESSAGE_ID_BITS
    can_id = can_id | msg_id

    return can_id

def dissect_can_id(can_id):
    """
    Splits an 11 bit CAN ID into a 4 bit device ID and 7 bit message ID.
    Message bits are in format: DDDDMMMMMMM

    Message ID retrieved by masking away the first 4 bits. Device ID retrieved
    by shifting away to the right the message ID bits.

    Returns tuple of 4 bit device ID and 7 bit message ID as numbers.
    """
    msg_id = can_id & MESSAGE_ID_MASK
    dev_id = can_id >> MESSAGE_ID_BITS

    return dev_id, msg_id
