/*
 * File:   main.c
 * Author: Derek Gutheil <dkg8689@rit.edu>
 *
 * Created on September 8, 2016
 */


#include "MC_register_defaults.h" /* Derek 12/8/2016: I am not sure if this file needs to be included in main or just in the project for the special pragma functions to work */
#include "taskManager.h"

void main(void)
{
    taskManager_init();

    //Main program loop
    while(1)
    {
        //Do nothing forever
    }
}