/* 
 * File:   app.c
 *
 * Created on September 8, 2016, 8:07 PM
 */

#include "app.h"

void app_init()
{
    //Initialize the app layer
}

void app_1ms()
{
    //Run any app specific 1ms cyclic tasks
    ;
}

void app_10ms()
{
    //Run any app specific 10ms cyclic tasks
    ;
}

void app_100ms()
{
    //Run any app specific 100ms cyclic tasks
    ;
}

void app_1000ms()
{
    //Run any app specific 1000ms cyclic tasks
    ;
}