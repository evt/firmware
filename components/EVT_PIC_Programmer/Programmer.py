#!/usr/bin/python3
import serial
import sys
from time import sleep

BAUDRATE = 9600
SERIALPORT = "/dev/ttyUSBO"
READYCHAR = b'?'
SENDCHAR = b'r'

def main(argv=None):
	# Parse command line arguments

	if (len(sys.argv) <= 1):
		raise Exception("Please input a serial device")
	if (len(sys.argv) <= 2):
		raise Exception("Please input an input file")
	
	SERIALPORT = "/dev/ttyUSB0" # default Serial port
	SERIALPORT = sys.argv[1] # get usb interface from cmd line input
	filename = sys.argv[2] # get from cmd line input

	#open file
	file_object = open(filename, 'r')
	
	#open Serial port
	ser = serial.Serial(port = SERIALPORT, baudrate=BAUDRATE, timeout=0.1)
	ser.close()
	ser.open()
	if ser.isOpen():
		print("Serial connection opened")
	else:
		print("Unable to open serial connection")
		return 1

	# conditional check to make sure BMS is ready to recieve hex
	response = None;
	if ser.isOpen():
		while (response != SENDCHAR):
			ser.write(READYCHAR)
			print("Writing: " + str(READYCHAR))
			sleep(0.1)
			response = ser.read()
			print("response: " + str(response))
		for char in iter(lambda: file_object.read(1), ''):
			ser.write(bytes(char, 'UTF-8'))
			print("Serial writing: " + char)

		#send the null char to end the transmission
		ser.write(bytes('\0', 'UTF-8'))

	# todo: calculate and send checksum

	response = ser.read()
	while (response != b''):
		print("Read char: " + str(response))
		response = ser.read()


	#Close serial connection and input file
	file_object.close()
	ser.close()

	return 0

if __name__ == "__main__":
	sys.exit(main())