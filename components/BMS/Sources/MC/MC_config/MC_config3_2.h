/* 
 * File:   MC_config3_2.h
 * Author: Derek Gutheil <dkg8689@rit.edu>
 *
 * Created on October 13, 2016
 */

#ifndef MC_CONFIG3_2_H
#define MC_CONFIG3_2_H
#include "MC_GPIO.h"



/* ===========================================================================================================================
 * Public precompiler definitions
 * ===========================================================================================================================
*/

/* ===========================================================================================================================
 * Public Enumerated type definitions
 * ===========================================================================================================================
*/

typedef enum MC_BOARD_E
{
    MC_BOARD_CS_ADC      = MC_PIN_2,
    MC_BOARD_ADC_RESET   = MC_PIN_3,
    MC_BOARD_TOS         = MC_PIN_4,
    MC_BOARD_CS_P        = MC_PIN_5,
    MC_BOARD_DRDY        = MC_PIN_7,
    MC_BOARD_CLKOUT      = MC_PIN_10,
    MC_BOARD_CLKIN       = MC_PIN_9,
    MC_BOARD_CONFIG_U    = MC_PIN_11,
    MC_BOARD_CONFIG_L    = MC_PIN_12,
    MC_BOARD_EFLAG       = MC_PIN_13,
    MC_BOARD_SCKI_P      = MC_PIN_14,
    MC_BOARD_SDI_P       = MC_PIN_15,
    MC_BOARD_SDO_P       = MC_PIN_16,
    MC_BOARD_CANTX       = MC_PIN_17,
    MC_BOARD_CANRX       = MC_PIN_18,
    MC_BOARD_IGNIT       = MC_PIN_21,
    MC_BOARD_KEY         = MC_PIN_22,
    MC_BOARD_PWDN        = MC_PIN_23,
    MC_BOARD_VMODE       = MC_PIN_24,
    MC_BOARD_ADCSTART    = MC_PIN_25,
    MC_BOARD_UNUSED      = MC_PIN_26,
    MC_BOARD_UART_TX     = MC_PIN_27,
    MC_BOARD_PGC         = MC_PIN_27,
    MC_BOARD_UART_RX     = MC_PIN_28,
    MC_BOARD_PGD         = MC_PIN_28,
    MC_BOARD_RESET       = MC_PIN_1
} MC_BOARD_E;


/* ===========================================================================================================================
 * Public Structure definitions
 * ===========================================================================================================================
*/


/* ===========================================================================================================================
 * Public function declarations
 * ===========================================================================================================================
*/

#endif    /* MC_CONFIG3_2_H */