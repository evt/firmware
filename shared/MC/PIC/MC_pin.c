/* 
 * File:   MC_pin.c
 *
 * Created on December 12, 2016, 8:40 PM
 */

#include "MC_pin.h"
#include "MC_processor.h"

/* ===========================================================================================================================
 * Private module variables
 * ===========================================================================================================================
*/

/* ===========================================================================================================================
 * Private module function declarations
 * ===========================================================================================================================
*/

/* ===========================================================================================================================
 *  Module function definitions
 * ===========================================================================================================================
*/
int8_t MC_pin_setDirection(MC_PIN_E pin, MC_PIN_DIRECTION_E direction)
{
    if (pin >= MC_PIN_NUM_PINS)
    {
        return -1;
    }
    else if (direction > MC_PIN_NUM_DIRECTIONS)
    {
        return -1;
    }
    else
    {
        #ifdef _18F25K80
        switch(pin)
        {
            case MC_PIN_NULL:
                return -1;
            case MC_PIN_RE3:
                return -1;
            case MC_PIN_RA0:
                ANSEL0 = MC_PIN_DIGITAL;
                TRISA0 = direction;
                break;      
            case MC_PIN_RA1:
                ANSEL1 = MC_PIN_DIGITAL;
                TRISA1 = direction;
                break;
            case MC_PIN_RA2:
                ANSEL2 = MC_PIN_DIGITAL;
                TRISA2 = direction;
                break;
            case MC_PIN_RA3:
                ANSEL3 = MC_PIN_DIGITAL;
                TRISA3 = direction;
                break;
            case MC_PIN_6:       
                return -1;
            case MC_PIN_RA5: 
                ANSEL4 = MC_PIN_DIGITAL;
                TRISA5 = direction;
                break;
            case MC_PIN_8:
                return -1;
            case MC_PIN_RA7:      
                TRISA7 = direction;
                break;
            case MC_PIN_RA6:      
                TRISA6 = direction;
                break;
            case MC_PIN_RC0:      
                TRISC0 = direction;
                break;
            case MC_PIN_RC1:      
                TRISC1 = direction;
                break;
            case MC_PIN_RC2:           
                TRISC2 = direction;
                break;
            case MC_PIN_RC3:           
                TRISC3 = direction;
                break;
            case MC_PIN_RC4:      
                TRISC4 = direction;
                break;
            case MC_PIN_RC5:      
                TRISC5 = direction;
                break;
            case MC_PIN_RC6:      
                TRISC6 = direction;
                break;
            case MC_PIN_RC7:      
                TRISC7 = direction;
                break;
            case MC_PIN_19:      
                return -1;
            case MC_PIN_20:      
                return -1;
            case MC_PIN_RB0:
                ANSEL10 = MC_PIN_DIGITAL;
                TRISB0 = direction;
                break;
            case MC_PIN_RB1:
                ANSEL8 = MC_PIN_DIGITAL;
                TRISB1 = direction;
                break;
            case MC_PIN_RB2:      
                TRISB2 = direction;
                break;
            case MC_PIN_RB3:      
                TRISB3 = direction;
                break;
            case MC_PIN_RB4:
                ANSEL9 = MC_PIN_DIGITAL;
                TRISB4 = direction;
                break;
            case MC_PIN_RB5:      
                TRISB5 = direction;
                break;
            case MC_PIN_RB6:      
                TRISB6 = direction;
                break;
            case MC_PIN_RB7:
                TRISB7 = direction;
                break;
            default:
                return -1;
        }      
        #endif    /* _18F25K80 */
    }
    return 0;
}

MC_PIN_DIRECTION_E MC_pin_getDirection(MC_PIN_E pin)
{
    if (pin >= MC_PIN_NUM_PINS)
    {
        return MC_PIN_DIRECTION_ERROR;
    }
    else
    {
        #ifdef _18F25K80
        switch(pin)
        {
            case MC_PIN_NULL:
                return MC_PIN_DIRECTION_ERROR;
            case MC_PIN_RE3:
                return MC_PIN_DIRECTION_ERROR;
            case MC_PIN_RA0:
                return (MC_PIN_DIRECTION_E)TRISA0;     
            case MC_PIN_RA1:
                return (MC_PIN_DIRECTION_E)TRISA1;
            case MC_PIN_RA2:       
                return (MC_PIN_DIRECTION_E)TRISA2;
            case MC_PIN_RA3:       
                return (MC_PIN_DIRECTION_E)TRISA3;
            case MC_PIN_6:       
                return MC_PIN_DIRECTION_ERROR;
            case MC_PIN_RA5:       
                return (MC_PIN_DIRECTION_E)TRISA5;
            case MC_PIN_8:
                return MC_PIN_DIRECTION_ERROR;
            case MC_PIN_RA7:      
                return (MC_PIN_DIRECTION_E)TRISA7;
            case MC_PIN_RA6:      
                return (MC_PIN_DIRECTION_E)TRISA6;
            case MC_PIN_RC0:      
                return (MC_PIN_DIRECTION_E)TRISC0;
            case MC_PIN_RC1:      
                return (MC_PIN_DIRECTION_E)TRISC1;
            case MC_PIN_RC2:           
                return (MC_PIN_DIRECTION_E)TRISC2;
            case MC_PIN_RC3:           
                return (MC_PIN_DIRECTION_E)TRISC3;
            case MC_PIN_RC4:      
                return (MC_PIN_DIRECTION_E)TRISC4;
            case MC_PIN_RC5:      
                return (MC_PIN_DIRECTION_E)TRISC5;
            case MC_PIN_RC6:      
                return (MC_PIN_DIRECTION_E)TRISC6;
            case MC_PIN_RC7:      
                return (MC_PIN_DIRECTION_E)TRISC7;
            case MC_PIN_19:      
                return MC_PIN_DIRECTION_ERROR;
            case MC_PIN_20:      
                return MC_PIN_DIRECTION_ERROR;
            case MC_PIN_RB0:      
                return (MC_PIN_DIRECTION_E)TRISB0;
            case MC_PIN_RB1:      
                return (MC_PIN_DIRECTION_E)TRISB1;
            case MC_PIN_RB2:      
                return (MC_PIN_DIRECTION_E)TRISB2;
            case MC_PIN_RB3:      
                return (MC_PIN_DIRECTION_E)TRISB3;
            case MC_PIN_RB4:
                return (MC_PIN_DIRECTION_E)TRISB4;
            case MC_PIN_RB5:      
                return (MC_PIN_DIRECTION_E)TRISB5;
            case MC_PIN_RB6:      
                return (MC_PIN_DIRECTION_E)TRISB6;
            case MC_PIN_RB7:
                return (MC_PIN_DIRECTION_E)TRISB7;
            default:
                return MC_PIN_DIRECTION_ERROR;
        }
        #else
        return MC_PIN_DIRECTION_ERROR;
        #endif    /* _18F25K80 */
    }
}