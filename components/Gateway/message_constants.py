
"""
Required device and message names for dictionary lookups
"""

import os

# File path of C header containing message definitions relative to this file
CAN_MESSAGES_HEADER_PATH = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    "../../shared/CANInterface/",
    'CANMessages.h'
)

# Device names
GTW_DEV = "GTW"

# Gateway message names
REQUEST_TRANSMIT_DATA_MSG = "GTW_REQUEST_TRANSMIT_DATA"
REQUEST_NAME_FIRMWARE_VERSION_MSG = "GTW_REQUEST_NAME_FIRMWARE_VERSION"
REQUEST_DEVICE_DATA_TYPES_MSG = "GTW_REQUEST_DEVICE_DATA_TYPES"

# Data Sender message names
NAME_MSG = "DS_NAME"
FIRMWARE_VERSION_MSG = "DS_FIRMWARE_VERSION"
DATA_TYPE_MSG = "DS_DATA_TYPE"
DS_DATA_START_MSG = "DS_DATA_START"
DS_DATA_END_MSG = "DS_DATA_END"
