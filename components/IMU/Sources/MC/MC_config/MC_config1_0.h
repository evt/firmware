/* 
 * File:   MC_config1_0.h
 *
 * Created on October 13, 2016
 */

#ifndef MC_CONFIG1_0_H
#define MC_CONFIG1_0_H
#include "MC_GPIO.h"



/* ===========================================================================================================================
 * Public precompiler definitions
 * ===========================================================================================================================
*/

/* ===========================================================================================================================
 * Public Enumerated type definitions
 * ===========================================================================================================================
*/

typedef enum MC_BOARD_E
{
    MC_BOARD_ALT_INT1    = MC_PIN_3,
    MC_BOARD_ALT_INT2    = MC_PIN_4,
    MC_BOARD_BMG_INT1    = MC_PIN_5,
    MC_BOARD_BMG_INT2    = MC_PIN_7,
    MC_BOARD_LED4        = MC_PIN_9,
    MC_BOARD_I2C_RESET   = MC_PIN_13,
    MC_BOARD_I2C_CLOCK   = MC_PIN_14,
    MC_BOARD_I2C_DATA    = MC_PIN_15,
    MC_BOARD_I2C_WAKE    = MC_PIN_16,
    MC_BOARD_CANTX       = MC_PIN_17,
    MC_BOARD_CANRX       = MC_PIN_18,
    MC_BOARD_BMC_INT1    = MC_PIN_21,
    MC_BOARD_BMC_INT2    = MC_PIN_22,
    MC_BOARD_BMC_INT3    = MC_PIN_23,
    MC_BOARD_P24         = MC_PIN_24,
    MC_BOARD_I2C2_ALERT  = MC_PIN_25,
    MC_BOARD_UART_RX     = MC_PIN_27,
    MC_BOARD_UART_TX     = MC_PIN_28,
    MC_BOARD_RESET       = MC_PIN_1,
    MC_BOARD_MCLR        = MC_PIN_1
} MC_BOARD_E;


/* ===========================================================================================================================
 * Public Structure definitions
 * ===========================================================================================================================
*/


/* ===========================================================================================================================
 * Public function declarations
 * ===========================================================================================================================
*/

#endif    /* MC_CONFIG1_0_H */