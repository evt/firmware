/* 
 * File:   taskManager.h
 * Author: Derek Gutheil <dkg8689@rit.edu>
 *
 * Created on September 8, 2016
 */

#ifndef TASKMANAGER_H
#define    TASKMANAGER_H

/*First thing to get called from boot*/
void taskManager_init();

void taskManager();

#endif    /* TASKMANAGER_H */

