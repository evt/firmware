/* 
 * File:   bootloader.h
 * Author: James Moretti
 */

#ifndef BOOTLOADER_H
#define    BOOTLOADER_H

//#include "serial.h"
//#include <flash.h>

#define UPDATE_AVAILABLE_FLAG ('?') // Read this from PC when an update is available
#define UPDATE_ACCEPT_FLAG ('?')    // Send this back to PC to confirm update download

/* This is also set inside the properties of both the BMS and Bootloader MPlabX projects*/
#define PROGRAM_START (0x400)    // Address a downloaded program is offset to

/***********************************************************************************
* Name        : bootload
* Description : Allows us to connect to PC and flash onboard memory with program updates.
* Pre         : None
* @param      : None
* @return     : None
***********************************************************************************/
void bootload();

/***********************************************************************************
* Name        : blinkHello
* Description : Blinks a light on the PIC to indicate successful boot
* Pre         : None
* @param      : None
* @return     : None
***********************************************************************************/
void blinkHello();

#endif    /* BOOTLOADER_H */

