#!/usr/bin/env python3

"""
This is the main entry file for the Gateway. It performs all initialization
required and begins the application.
"""

import os
import sys

import bus_master
import c_header_parser
import logger

from message_constants import CAN_MESSAGES_HEADER_PATH, GTW_DEV

def main():
    """
    Perform initializations and tell CAN bus to start listening for messages.
    """
    header = c_header_parser.CHeader()
    header.parse(CAN_MESSAGES_HEADER_PATH)
    gtw_id = header.get_device_id(GTW_DEV)
    local_logger = logger.LocalLogger("./log.csv")
    can_bus = bus_master.BusMaster(gtw_id, header, logger=local_logger)
    can_bus.init_bus()
    can_bus.listen()
    return 0

if __name__ == '__main__':
    sys.exit(main())
