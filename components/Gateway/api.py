import requests

BIKE_METRICS_URL = "https://evt-metrics.rit.edu"

def post_data():
    pass

def get_voltage(**kwargs):
    url = BIKE_METRICS_URL + "/"
    return _get_json(url, **kwargs)

# Current isn't being used
def get_current(**kwargs):
    url = BIKE_METRICS_URL + "/"
    return _get_json(url, **kwargs)

def get_temperature(**kwargs):
    url = BIKE_METRICS_URL + "/"
    return _get_json(url, **kwargs)

def _get_json(url, **kwargs):
    r = requests.get(url, params=kwargs)
    if r.status_code != requests.codes.ok:
        r.raise_for_status()

    return r.json()
