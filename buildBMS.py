#!/usr/bin/python
import os

if (os.name == "posix"):
	os.system("/opt/microchip/mplabx/mplab_ide/bin/prjMakefilesGenerator.sh components/BMS")
	os.chdir("components/BMS")
	os.system("make")
else:
	os.system('"C:\\Program Files (x86)\\Microchip\\MPLABX\\mplab_ide\\bin\\prjMakefilesGenerator.bat" components/BMS')
	os.chdir("components/BMS")
	os.system('"C:\\Program Files (x86)\\Microchip\\MPLABX\\gnuBins\\GnuWin32\\bin\\make"')