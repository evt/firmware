/*
 * File:    MC_SPI.h
 * Created: 3/24/2017
*/

#ifndef MC_SPI_H
#define MC_SPI_H

#include "EVT_std_includes.h"

/* ===========================================================================================================================
 * Public precompiler definitions
 * ===========================================================================================================================
*/


/* ===========================================================================================================================
 * Public Enumerated type definitions
 * ===========================================================================================================================
*/

typedef enum mc_spi_sync_modes_E
{
    MC_SPI_SYNC_DEFAULT = 0u,
    MC_SPI_FOSC_4    = 0u,      // SPI Master mode, clock = Fosc/4
    MC_SPI_FOSC_16   = 1u,      // SPI Master mode, clock = Fosc/16
    MC_SPI_FOSC_64   = 2u,      // SPI Master mode, clock = Fosc/64
    MC_SPI_FOSC_TMR2 = 3u,      // SPI Master mode, clock = TMR2 output/2
    MC_SLV_SSON      = 4u,      // SPI Slave mode, /SS pin control enabled
    MC_SLV_SSOFF     = 5u,      // SPI Slave mode, /SS pin control disabled
    MC_SPI_FOSC_8    = 10u,     // SPI Master mode, clock = Fosc/8
} mc_spi_sync_modes_E;

typedef enum mc_spi_bus_modes_E
{
    MC_SPI_MODE_DEFAULT = 0u,
    
    /* Setting for SPI bus Mode 0,0
     *     CKE = 1 Transmit occurs on transition from active to Idle clock state  // SSPSTAT register 
     *     CKP = 0 Idle state for clock is a low level                            // SSPCON1 register
     */
    MC_SPI_MODE_00      = 0u,
    
    /* Setting for SPI bus Mode 0,1
     *     CKE = 0 Transmit occurs on transition from Idle to active clock state  // SSPSTAT register 
     *     CKP = 0 Idle state for clock is a low level                            // SSPCON1 register
     */
    MC_SPI_MODE_01      = 1u,
    
    /* Setting for SPI bus Mode 1,0
     *     CKE = 1 Transmit occurs on transition from active to Idle clock state  // SSPSTAT register
     *     CKP = 1 Idle state for clock is a high level                           // SSPCON1 register
     */
    MC_SPI_MODE_10      = 2u,
    
    /* Setting for SPI bus Mode 1,1
     *     CKE = 0 Transmit occurs on transition from Idle to active clock state  // SSPSTAT register
     *     CKP = 1 Idle state for clock is a high level                           // SSPCON1 register
     */
    MC_SPI_MODE_11      = 3u,
} mc_spi_bus_modes_E;

// Master SPI mode only
typedef enum mc_spi_sample_modes_E
{
    MC_SPI_SAMPLE_DEFAULT = 0u,
    MC_SPI_SAMPLE_MID     = 0u,   // Input data sample at middle of data out
    MC_SPI_SAMPLE_END     = 128u, // Input data sample at end of data out
} mc_spi_sample_modes_E;

/* ===========================================================================================================================
 * Public Structure definitions
 * ===========================================================================================================================
*/


/* ===========================================================================================================================
 * Public function declarations
 * ===========================================================================================================================
*/

/* These 3 functions set the configuration parameters that are needed to open the SPI port.
 * These MUST be set before calling mc_spi_open() or it will open a port with undefined properties
 */
void mc_spi_setRequestedSyncMode(mc_spi_sync_modes_E syncMode);
void mc_spi_setRequestedBusMode(mc_spi_bus_modes_E busMode);
void mc_spi_setRequestedSampleMode(mc_spi_sample_modes_E sampleMode);


/***********************************************************************************
* Name        : mc_spi_open
* Description : opens SPI at the mode defined by mc_spi_setRequestedSyncMode, mc_spi_setRequestedBusMode, and mc_spi_setRequestedSampleMode
* Pre         : mc_spi_setRequestedSyncMode, mc_spi_setRequestedBusMode, and mc_spi_setRequestedSampleMode are set
* @param      : None
* @return     : None
***********************************************************************************/
void mc_spi_open();

/***********************************************************************************
* Name        : mc_spi_close
* Description : closes SPI. Sets pins back as GPIO. Clears the values set by mc_spi_setRequestedSyncMode, mc_spi_setRequestedBusMode, and mc_spi_setRequestedSampleMode
* @param      : None
* @return     : None
***********************************************************************************/
void mc_spi_close();

/***********************************************************************************
* Name        : mc_spi_reopen
* Description : closes SPI, sets the module variables for configuring SPI, re-opens spi to the new configuration
* @param      : SPI config parameters
* @return     : None
***********************************************************************************/
void mc_spi_reopen(mc_spi_sync_modes_E syncMode, mc_spi_bus_modes_E busMode, mc_spi_sample_modes_E sampleMode);

/***********************************************************************************
* Name        : mc_spi_readChar
* Description : Read single byte from SPI bus.
* Pre         : SPI is initialized
* @param      : void
* @return     : contents of SSPBUF register
***********************************************************************************/
uint8_t mc_spi_readChar();

/***********************************************************************************
* Name        : mc_spi_writeChar
* Description : This routine writes a single byte to the SPI bus.
* Pre         : SPI is initialized
* @param      : data_out: Single data byte for SPI bus.
* @return     : Status byte for WCOL detection.
***********************************************************************************/
int8_t mc_spi_writeChar(uint8_t data_out);

#endif 	/* MC_SPI_H */