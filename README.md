[![Build Status](http://evt-jenkins.rit.edu:8080/buildStatus/icon?job=BMS)](http://evt-jenkins.rit.edu:8080/job/BMS/)

Firmware
========

Welcome the Rochester Institute of Technology Electric Vehicle Team Firmware repository. This repository holds all of the firmware for our electric motorcycle that will be competing in the 2015 eMotoRacing series.
Feel free to look around, create an issue, leave a comment, etc. 
---
License: [MIT License](http://opensource.org/licenses/MIT)