/*
 * File:   MC_timer.c
 *
 * Created on September 9, 2016, 8:07 PM
 * Edited on March 19, 2017 by Jacob Martinez & Joseph Rondinelli
 */
#include "MC_timer.h"
#include "timers.h" //included for the OpenTimer4 function. Function in implemented in t4open.c though

/* ========================================================================
 * Module Function Declarations
 * ========================================================================
*/
void timer_initSubFun(mc_timer_interrupt_periods_E interrupt_value, uint8_t* scalerValues, uint8_t* periodRegister);

/* ========================================================================
 * Module Function Definitions
 * ========================================================================
*/
void timer_initSubFun(mc_timer_interrupt_periods_E interrupt_value, uint8_t* scalerValues, uint8_t* periodRegister)
{
    /* Timer frequency is defined as follows:
    * 
    * FreqOSC is at 8 MHZ
    * FreqT = (1/PreScaler) * (FreqOSC/4) * (1/PostScaler)
    *
    * With FreqOSC being the frequency of the internal or external oscillator,
    * PreScaler is the first argument, and Post is the second*/
    switch(interrupt_value)
    {
    	case MC_TIMER_100US:
        	*scalerValues = T2_PS_1_1 & T2_POST_1_8;
            *periodRegister = 25;
        	break;
    	case MC_TIMER_200US:
        	*scalerValues = T2_PS_1_1 & T2_POST_1_8;
            *periodRegister = 50;
        	break;
    	case MC_TIMER_300US:
        	*scalerValues = T2_PS_1_1 & T2_POST_1_8;
            *periodRegister = 75;
        	break;
    	case MC_TIMER_400US:
        	*scalerValues = T2_PS_1_1 & T2_POST_1_8;
            *periodRegister = 100;
        	break;
    	case MC_TIMER_500US:
        	*scalerValues = T2_PS_1_1 & T2_POST_1_8;
            *periodRegister = 125;
        	break;
    	case MC_TIMER_600US:
        	*scalerValues = T2_PS_1_1 & T2_POST_1_8;
            *periodRegister = 150;
        	break;
    	case MC_TIMER_700US:
        	*scalerValues = T2_PS_1_1 & T2_POST_1_8;
            *periodRegister = 175;
        	break;
    	case MC_TIMER_800US:
        	*scalerValues = T2_PS_1_1 & T2_POST_1_8;
            *periodRegister = 200;
        	break;
    	case MC_TIMER_900US:
        	*scalerValues = T2_PS_1_1 & T2_POST_1_8;
            *periodRegister = 225;
        	break;
    	case MC_TIMER_1MS:
        	*scalerValues = T2_PS_1_1 & T2_POST_1_8;
            *periodRegister = 250;
        	break;
    	case MC_TIMER_2MS:
        	*scalerValues = T2_PS_1_1 & T2_POST_1_16;
            *periodRegister = 250;
        	break;
    	case MC_TIMER_4MS:
        	*scalerValues = T2_PS_1_4 & T2_POST_1_8;
            *periodRegister = 250;
        	break;
    	case MC_TIMER_8MS:
        	*scalerValues = T2_PS_1_4 & T2_POST_1_16;
            *periodRegister = 250;
        	break;
    	case MC_TIMER_16MS:
        	*scalerValues = T2_PS_1_16 & T2_POST_1_8;
            *periodRegister = 250;
        	break;
	}
}

void mc_timer_init(mc_timer_E timer, mc_timer_interrupt_periods_E interrupt_value)
{
    uint8_t scalerValues;
    uint8_t periodRegister;
    switch (timer)
	{	 
        case MC_TIMER_2:
            timer_initSubFun(interrupt_value, &scalerValues, &periodRegister);
        	OpenTimer2(scalerValues);
        	PR2bits.PR2 = periodRegister;
        	break;

    	case MC_TIMER_4:
        	timer_initSubFun(interrupt_value, &scalerValues, &periodRegister);
        	OpenTimer4(scalerValues);
        	PR4bits.PR4 = periodRegister;
        	break;    
	}
}
uint8_t mc_timer_getStatus(mc_timer_E timer)
{
    uint8_t returnVal = 0;
    switch(timer)
    {
      	case MC_TIMER_2:
          	returnVal = TMR2;
      	    break;
      	case MC_TIMER_4:
          	returnVal = TMR4;
      	    break;
  	}
    return returnVal;
}