/*
 * File: app_networkManager.h
 * Created: 4/2/2017
 *
 * Responsible for collecting data from other applications and packaging the
 * data into formatted frames for transmission on the CAN bus.
*/

#ifndef APP_NETWORKMANAGER_H
#define APP_NETWORKMANAGER_H

#include <EVT_std_includes.h>

/* ============================================================================
 * Public Enumerated Type Definitions
 * ============================================================================
*/


/* ============================================================================
 * Public Structure Definitions
 * ============================================================================
*/


/* ============================================================================
 * Public Function Declarations
 * ============================================================================
*/

void app_networkManager_1ms();

void app_networkManager_10ms();

void app_networkManager_100ms();

#endif /* APP_NETWORKMANAGER_H */
