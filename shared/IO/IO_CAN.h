/* 
 * File:   IO_CAN.h
 * Author: Derek Gutheil <dkg8689@rit.edu>
 *
 * Created on September 8, 2016, 8:07 PM
 */

#ifndef IO_CAN_H
#define IO_CAN_H

#include "EVT_std_includes.h"

/* ===========================================================================================================================
 * Public precompiler definitions
 * ===========================================================================================================================
*/

/* ===========================================================================================================================
 * Public Enumerated type definitions
 * ===========================================================================================================================
 */


/* ===========================================================================================================================
 * Public Structure definitions
 * ===========================================================================================================================
 */

typedef struct io_can_message_S
{
    uint16_t address;  
    uint8_t data[8];
    /* numBytes must be <= 8! */
    uint8_t numBytes             :4;
} io_can_message_S;

/* ===========================================================================================================================
 * Public function declarations
 * ===========================================================================================================================
 */
void io_can_init();

uint8_t io_can_transmitMessage(io_can_message_S * message);
void io_can_getMessage(io_can_message_S * returnMessage);
uint8_t io_can_bufferIsEmpty();

#endif    /* IO_CAN_H */

