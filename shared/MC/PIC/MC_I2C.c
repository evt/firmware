/*
 * File:    MC_I2C.c
 * Date:    11/4/16
 */

#include "MC_I2C.h"
#include "MC_processor.h"

/* =============================================================================
 * Private Module constant definitions
 * =============================================================================
 */
#define SSPM_I2C_7BIT_SLAVE     (0x06)
#define SSPM_I2C_MASTER         (0x08)

/* Section 21.4.7 of the PIC data-sheet discusses I2C baud rates and provides
 * tables to help determine the proper register values required to obtain a 
 * specific baud rate.
 */
#define BAUD_REGISTER_31KHZ    (0x13)

/* ===========================================================================================================================
 * Private module variables
 * ===========================================================================================================================
 */

uint8_t I2CBaudRegisterVal;
int8_t I2CSlaveAddress;
bool I2CIsMaster;

/* ===========================================================================================================================
 * Private module function declarations
 * ===========================================================================================================================
 */

/* ===========================================================================================================================
 *  Module function definitions
 * ===========================================================================================================================
 */

/* Initializes the MSSP for I2C communications. */
void MC_I2C_Init()
{
    /*Set the direction of the SDA pin*/
    TRISC3 = 1;
    /*Set the direction of the SCL pin*/
    TRISC4 = 1;

    /*Set the config registers to 0 so that we can set the bits we want
     * while keeping the others clear.
     */
    SSPCON1 = 0x0;
    SSPCON2 = 0x0;

    /*Disable and clear interrupts from the MSSP*/
    PIE1bits.SSPIE = 0;
    PIR1bits.SSPIF = 0;

    /*Disable slew rate control*/
    SSPSTATbits.SMP = 1;

    /*Disable SMBus specific inputs*/
    SSPSTATbits.CKE = 0;

    /*Default to 7-bit address slave mode*/
    SSPCON1bits.SSPM = SSPM_I2C_MASTER;

    /*Load an initial value for the baud register*/
    I2CBaudRegisterVal = BAUD_REGISTER_31KHZ;

    /*Initially there is no slave address*/
    I2CSlaveAddress = -1;
    
    I2CIsMaster = false;
    
    /*Enable the MSSP and configure the SDA and SDL pins*/
    SSPCON1bits.SSPEN = 1;
}

MC_I2C_BAUD_E MC_I2C_SetBaud(MC_I2C_BAUD_E baud)
{
    /*TODO: Get this puppy working as advertised. */
    
    I2CBaudRegisterVal = BAUD_REGISTER_31KHZ;

    return MC_I2C_100KHZ;
}

MC_I2C_BAUD_E MC_I2C_GetBaud()
{
    /*The baud rate we're interested in*/
    uint8_t baud;
    
    /*If we're in the middle of a transfer, give the current baud rate*/
    if (I2CIsMaster)
    {
        baud = SSPADD;
    }
    
    /*Otherwise provide the baud rate for the next transfer*/
    else
    {
        baud = I2CBaudRegisterVal;
    }
    
    /*Get the correct enum based on the baud rate*/
    switch (baud)
    {
        case BAUD_REGISTER_31KHZ:
            return MC_I2C_31KHZ;
        
        /*The baud value was either invalid or unsupported*/
        default:
            return MC_I2C_BAUD_ERROR;
    }
}

void MC_I2C_SetSlaveAddr(int8_t addr)
{
    if (addr <= 0)
    {
        /*The address was not valid, so there will be no slave mode*/
        I2CSlaveAddress = -1;
    }
    
    else
    {
        /*Set the slave address to the provided 7-bit address*/
        I2CSlaveAddress = addr;
        
        /*If we are not in the middle of the transmit*/
        if (!I2CIsMaster)
        {
            /*Change our address immediately*/
            SSPADD = addr;
            /*Switch to slave mode if we weren't already there*/
            SSPCON1bits.SSPM = SSPM_I2C_7BIT_SLAVE;
        }
    }
}

int8_t MC_I2C_GetSlaveAddr()
{
    return I2CSlaveAddress;
}

/* Sets the MSSP to master transmit mode and generates a START bit. 
 * Requires that the P bit in the SSPSTAT register is set, indicating
 * the last thing sent on the bus was a STOP bit.*/
void MC_I2C_START()
{
    /*Load the baud rate for the next transfer*/
    SSPADD = I2CBaudRegisterVal; /* this may need to be a line later*/
    
    /*MSSP Mode select as master*/
    /*clock = FOSC/(4 * (SSPADD + 1))*/
    SSPCON1bits.SSPM = SSPM_I2C_MASTER;

    /*Send the START bit*/
    SSPCON2bits.SEN = 1;
 
    /*Since we're sending out the START, we are the master*/
    I2CIsMaster = true;
    
    while (SSPCON2bits.SEN) 
    {
        ; /* Wait for the START condition to finish. */
    }
}

void MC_I2C_reSTART()
{
    /*Set the condition for a repeated start*/
    SSPCON2bits.RSEN = 1;
    while (SSPCON2bits.RSEN)
    {
        ; /* Wait for repeated START condition to finish. */
    }
}

void MC_I2C_STOP()
{
    /* Generate STOP condition */
    SSPCON2bits.PEN = 1;
    while (SSPCON2bits.PEN)
    {
        ; /* Wait for STOP condition to finish */
    }
    
    /* If the I2C module has been supplied with a valid slave address,
     * then switch to slave mode so that the module can receive incoming
     * requests. Otherwise, stay in master mode as an inactive master.
     */
    if (I2CSlaveAddress > 0) 
    {
        /* Restore SSPADD to the slave address */
        SSPADD = I2CSlaveAddress;
        /* Return to 7-bit slave mode */
        SSPCON1bits.SSPM = SSPM_I2C_7BIT_SLAVE;
    }
    
    /*We are no longer the master*/
    I2CIsMaster = false;
}

void MC_I2C_ACK()
{
    /*Set the MSSP to send an ACK*/
    SSPCON2bits.ACKDT = 0;

    /*Send the ACK bit*/
    SSPCON2bits.ACKEN = 1;
    while (SSPCON2bits.ACKEN)
    {
        ; /* Wait for the ACK to finish */
    }
}

void MC_I2C_NACK()
{
    /*Set the MSSP to send a NACK*/
    SSPCON2bits.ACKDT = 1;

    /*Send the NACK bit*/
    SSPCON2bits.ACKEN = 1;
    while (SSPCON2bits.ACKEN) 
    {
        ; /* Wait for the NACK to finish. */
    }
}

int8_t MC_I2C_Tx(uint8_t data)
{
    /* Write to the SSP data buffer */
    SSPBUFbits.SSPBUF = data;

    /* Check to see if a write collision occurred. */
    if (SSPCON1bits.WCOL)
    {
        return -1;
    }

    return 0;
}

uint8_t MC_I2C_Rx()
{
    /* Read from the SSP data buffer */
    return SSPBUFbits.SSPBUF;
}

bool MC_I2C_TxRdy()
{
    /* Check that the SSP data buffer is empty */
    return (bool) !(SSPSTATbits.BF);
}

bool MC_I2C_RxRdy()
{
    /* Check that the SSP data buffer is full */
    return (bool) SSPSTATbits.BF;
}

bool MC_I2C_TxComplete()
{
    /* See if we're in master mode */
    if (SSPCON1bits.SSPM == SSPM_I2C_MASTER)
    {
        /* If we're in master mode we just need to check the BF flag. */
        return (bool) !(SSPSTATbits.BF);
    }

    /* Currently only master mode is supported. */

    else
    {
        /* This mode is invalid or not supported. */
        return false;
    }
}

bool MC_I2C_ACKReceived()
{
    return (bool) !(SSPCON2bits.ACKSTAT);
}

void MC_I2C_BeginRx()
{
    /* Initiates the reception of a single byte */
    SSPCON2bits.RCEN = 1;
}

bool MC_I2C_IsIdle()
{
    /* Checks that the I2C module is not in the process of
     *  +   an ACK/NACK
     *  +   a receive
     *  +   a STOP condition
     *  +   a repeated START condition
     *  +   a START condition
     *  +   a transmit
     */
    return (bool) !((SSPCON2 & 0x1F) || (SSPSTATbits.R_W));
}

bool MC_I2C_IsMaster()
{
    return I2CIsMaster;
}
